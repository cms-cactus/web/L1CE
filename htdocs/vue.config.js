const webpack = require('webpack');
const execa = require('execa');

const barecmd = (c, o) => execa.sync(c, o).stdout;
const cmd = (c, o) => JSON.stringify(barecmd(c, o));

const GIT_LATEST_TAG = cmd('git', ['describe', '--tags', '--abbrev=0']);

module.exports = {
  pwa: {
    name: 'l1ce',
    themeColor: '#0053a1',
    msTileColor: '#0053a1',
    manifestPath: 'site.webmanifest',
    iconPaths: {
      favicon32: 'img/favicon-32x32.png',
      favicon16: 'img/favicon-16x16.png',
      appleTouchIcon: 'img/apple-touch-icon-152x152.png',
      maskIcon: 'img/safari-pinned-tab.svg',
      msTileImage: 'img/mstile-144x144.png'
    }
  },
  configureWebpack: {
    performance: {
      hints: false,
    },
    plugins: [
      new webpack.DefinePlugin({
        GIT_LATEST_TAG,
        GIT_TAG_TRAILING_COMMITS: Number(barecmd('git', ['rev-list', JSON.parse(GIT_LATEST_TAG) + '..HEAD', '--count'])),
        GIT_HASH: cmd('git', ['rev-parse', '--short', 'HEAD']),
        GIT_BRANCH: cmd('git', ['rev-parse', '--abbrev-ref', 'HEAD']),
        GIT_UNCOMMITTED_FILES: cmd('git', ['status', '-s']) !== '',

        BUILD_TIMESTAMP: cmd('date', ['--rfc-3339=seconds']),
        BUILD_HOST: cmd('hostname', ['--long']),
        BUILD_FROM_CI: !!process.env.CI,
        BUILD_UNAME: cmd('uname', '-a'),
        BUILD_KERNEL: cmd('uname', '--kernel-release'),
        BUILD_ARCH: cmd('uname', '--machine'),
      })
    ]
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/style/theme.scss";
          @import "~vue-material/src/components/MdAnimation/variables";
          @import "~vue-material/src/components/MdAnimation/variables";
          @import "~vue-material/src/components/MdLayout/mixins";
          @import "~vue-material/src/components/MdElevation/mixins";
        `
      }
    }
  },
}
