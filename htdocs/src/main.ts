import Vue from 'vue';
import App from './App.vue';
import router from './lib/router';
import { store } from './store';
import './lib/registerServiceWorker';
import './lib/konami';
// import VueMaterial from 'vue-material';
import './style';

Vue.config.productionTip = false;

// Vue.use(VueMaterial);
export const vue = new Vue({
  router,
  store: store.vuexStore,
  render: h => h(App),
});

const bootMsgEl = document.getElementById('bootmsg') as HTMLElement;
bootMsgEl.textContent = 'authenticating';
store.dispatch.auth('init')
.then(() => {
  bootMsgEl.textContent += '\nauthentication done\nstarting interface';
  vue.$mount('#app');
})
.catch(err => {
  bootMsgEl.textContent = 'ERROR: ' + err.message;
});
