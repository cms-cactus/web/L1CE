import Vue, { WatchOptions } from 'vue';
import Vuex, { DispatchOptions, CommitOptions } from 'vuex';

import * as websocket from './websocket';
import * as auth from './auth';
import * as database from './database';
import * as editor from './editor';
import { StoreOpts, StoreModule } from './_types';

const storeOpts: StoreOpts<modules> = {
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    websocket: websocket.module,
    auth: auth.module,
    database: database.module,
    editor: editor.module,
  },
};

export type modules = 'websocket' | 'auth' | 'database' | 'editor';

export interface State {
  websocket: websocket.State;
  auth: auth.State;
  database: database.State;
  editor: editor.State;
}

const getCommand = (moduleName: modules, op: string) => {
  const module = storeOpts.modules[moduleName];
  return !module.namespaced ? op : moduleName + '/' + op;
};

const committer = <Mutations extends string>(moduleName: modules) =>
  (mutationName: Mutations, payload?: any, options?: CommitOptions) =>
    vuexStore.commit(getCommand(moduleName, mutationName), payload, options);

const dispatcher = <Actions extends string>(moduleName: modules) =>
  (actionName: Actions, payload?: any, options?: DispatchOptions) =>
    vuexStore.dispatch(getCommand(moduleName, actionName), payload, options);

const getter = <Getter extends string>(moduleName: modules) =>
  (getterName: Getter) => vuexStore.getters[getCommand(moduleName, getterName)];

Vue.use(Vuex);
const vuexStore = new Vuex.Store(storeOpts);
export const store = {
  vuexStore,

  state: vuexStore.state as State,

  dispatch: {
    websocket: dispatcher<websocket.actions>('websocket'),
    auth: dispatcher<auth.actions>('auth'),
    database: dispatcher<database.actions>('database'),
    editor: dispatcher<editor.actions>('editor'),
  },

  commit: {
    websocket: committer<websocket.mutations>('websocket'),
    auth: committer<auth.mutations>('auth'),
    database: committer<database.mutations>('database'),
    editor: committer<editor.mutations>('editor'),
  },

  get: {
    websocket: getter<websocket.getters>('websocket'),
    auth: getter<auth.getters>('auth'),
    database: getter<database.getters>('database'),
    editor: getter<editor.getters>('editor'),
  },

  watch: (watcher: (state: State, getters: any) => any,
          callback: (value: any, oldvalue: any) => void,
          options?: WatchOptions) => {
    return vuexStore.watch(watcher, callback, options);
  },
};
