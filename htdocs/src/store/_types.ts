import { DispatchOptions, CommitOptions } from 'vuex';

export interface StoreOpts<modules extends string> {
  strict?: boolean;
  modules: { [s in modules]: StoreModule<any> };
}

export interface StoreModule<S, M extends string = any, A extends string = any, G extends string = any> {
  namespaced?: boolean;
  readonly state: S;
  readonly mutations?: { [s in M]: (s: S, ...a: any) => any };
  readonly actions?: { [s in A]: (s: ActionContext<S, M, A, G>, ...a: any) => any };
  readonly getters?: { [s in G]: (s: S, ...a: any) => {} };
}

interface ActionContext<S, M extends string, A extends string, G extends string> extends StoreModule<S, M, A, G> {
  dispatch: (type: A, payload?: any, options?: DispatchOptions) => Promise<any>;
  commit: (type: M, payload?: any, options?: CommitOptions) => void;
}
