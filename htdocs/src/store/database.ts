import { StoreModule } from './_types';
import axios from '../lib/axios';
import { store } from './';
import eventBus from '../lib/eventBus';

export interface Change {
  author: string;
  keyName: string;
  sourceKeyName: string;
  tableName: string;
  tableOwner: string;
  type: 'new' | 'invalidation' | 'validation';
}

export interface State {
  subsystemTables: {
    [subsystem: string]: {
      blocks: {
        [sectionKey: string]: {
          alias: string;
          table: string;
        };
      };
    };
  };
  cache: {
    [id: string]: any;
  };
  changes: {
    [username: string]: Change[];
  };
}
export type connectionStates = 'offline' | 'connecting' | 'online' | 'error';
export type mutations = 'setSubsystemTables' | 'resetSubsystemTables' | 'clearCache' | 'addCache' | 'setChanges';
export type actions = 'getSubsystemTables' | 'cachedAxios' | 'getKeys' | 'getValidKeys' | 'getRow' | 'getChanges';
export type getters = 'mychanges';

const editorUrlRegex = /\/editor\/?([^\/]+)?\/?([^\/]+)?/;
export const module: StoreModule<State, mutations, actions, getters> = {
  namespaced: true,
  state: {
    subsystemTables: {},
    cache: {},
    changes: {},
  },
  mutations: {
    setSubsystemTables: (state, subsystemTables) => {
      state.subsystemTables = subsystemTables;
    },
    resetSubsystemTables: (state) => {
      state.subsystemTables = {};
    },
    clearCache: (state, key) => {
      if (!key) {
        state.cache = {};
      } else {
        delete state.cache[key];
      }
    },
    addCache: (state, [key, data]) => state.cache[key] = data,
    setChanges: (state, changes) => state.changes = changes,
  },
  actions: {
    getSubsystemTables: context => {
      if (Object.keys(context.state.subsystemTables).length === 0) {
        return axios.get('/api/v0/db/subsystems')
        .then(result => {
          delete result.data._links;
          context.commit('setSubsystemTables', result.data);
        });
      }
    },
    cachedAxios: (context, options) => {
      const url = options.url;
      const c: Promise<any> = context.state.cache[url];
      if (c) {
        return c;
      } else {
        const p = axios(options)
        .catch(err => {
          context.commit('clearCache', url);
          throw err;
        });
        context.commit('addCache', [url, p]);
        return p;
      }
    },
    getKeys: (context, [tableName, errorReporting = 'blocking']) => {
      const url = `/api/v0/db/tables/${tableName}/getkeys`;
      return context.dispatch('cachedAxios', {
        url,
        method: 'post',
        timeout: 60000,
        errorReporting,
      });
    },
    getValidKeys: (context, [tableName, sourceKey, errorReporting = 'blocking']) => {
      const url = `/api/v0/db/tables/${tableName}/${encodeURIComponent(sourceKey)}/getvalidkeys`;
      return context.dispatch('cachedAxios', {
        url,
        method: 'post',
        timeout: 60000,
        errorReporting,
      });
    },
    getRow: (context, [tableName, key, errorReporting = 'blocking']) => {
      const url = `/api/v0/db/tables/${tableName}/${encodeURIComponent(key)}`;
      return context.dispatch('cachedAxios', {
        url,
        timeout: 60000,
        errorReporting,
      });
    },
    getChanges: async context => {
      const response = await axios({url: '/api/v0/db/changes'});
      const changes = response.data;
      context.commit('setChanges', changes);
      return changes;
    },
  },
  getters: {
    mychanges: state => {
      const authData = store.state.auth.userData;
      if (!authData) {
        return [];
      }
      const username = authData.cern.fullname;
      return state.changes[username] || [];
    },
  },
};

eventBus.on('key-saved', event => {
  // bust getKeys cache
  const url = `/api/v0/db/tables/${event.table}/getkeys`;
  store.commit.database('clearCache', url);

  store.dispatch.database('getChanges');
});
