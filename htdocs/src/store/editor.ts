import { StoreModule } from './_types';
import { store } from './';
import router from '../lib/router';

export interface State {
  keyPath: KeyPathKey[];
  compareTable: string;
}
export type mutations = 'setKeyPath' | 'changeKeyPath' | 'setCompareTable';
export type actions = 'readURLData';
export type getters = '';

export interface KeyPathKey {
  tableName: string;
  key: string;
  options: {
    [name: string]: string;
  };
}

function updateURL(keyPath: KeyPathKey[]) {
  const routerPath = router.currentRoute.fullPath;

  if (routerPath.startsWith('/editor')) {
    const path = '/editor/' + keyPath
      .map((entry: KeyPathKey) => {
        const options = Object.keys(entry.options || {}).map(name => name + '=' + entry.options[name]);
        const pieces = [entry.tableName, entry.key];
        return pieces.filter(p => p).map(encodeURIComponent).concat(options).join('+');
      })
      .join('/');
    if (routerPath !== path) {
      router.push({path});
    }
  }
}

export const module: StoreModule<State, mutations, actions, getters> = {
  namespaced: true,
  state: {
    keyPath: [],
    compareTable: '',
  },
  mutations: {
    setKeyPath: (state, keyPath: KeyPathKey[]) => {
      state.keyPath = keyPath;
      updateURL(keyPath);
      if (keyPath.length !== 0) {
        state.compareTable = keyPath[0].tableName;
      }
    },
    setCompareTable: (state, table: string) => {
      state.compareTable = table;
    },
    changeKeyPath: (state, {position, entry, splice = true}: {position?: number, entry: any, splice: boolean}) => {
      if (!position && position !== 0) {
        position = state.keyPath.length - 1;
      }
      state.keyPath[position] = Object.assign(state.keyPath[position] || {}, entry);
      if (splice) {
        state.keyPath = state.keyPath.slice(0, position + 1);
      }
      updateURL(state.keyPath);
    },
  },
  actions: {
    readURLData: context => {
      const currentPath = window.location.pathname;

      if (currentPath.startsWith('/editor/')) {
        const tailPieces = currentPath.split('/').filter(s => s).slice(1);
        const keyPath = tailPieces.map(piece => {
          const [tableName, key, ...rest] = piece.split('+').map(decodeURIComponent);
          const options = rest.reduce((r, s) => {
            const [name, value] = s.split('=');
            r[name] = value;
            return r;
          }, {});
          return {tableName, key, options};
        });
        store.commit.editor('setKeyPath', keyPath);
      }
    },
  },
};
