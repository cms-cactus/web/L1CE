import { StoreModule } from './_types';
import axios from '../lib/axios';
import { store } from './';

const tokenKey = 'CERN Session';
const lastVisitKey = 'lastvisit_date';

export interface State {
  token: string | null;
  lastRenewDate: Date;
  userData: null | {
    l1ce: {
      mode: 'read' | 'write',
      database: 'development' | 'production',
      roles: string[],
    },
    cern: {
      fullname: string,
      name: string,
    },
    iat: number,
    exp: number,
  };
}
export type mutations = 'setToken';
export type actions = 'renew' | 'init' | 'logout';
export type getters = 'isReadMode' | 'isAdmin';

function getUserData(token: string): State['userData'] {
  if (!token) {
    return null;
  }
  let body = null;
  try {
    body = JSON.parse(atob(token.split('.')[1]));
    return body;
  } catch (e) {
    return null;
  }
}

window.addEventListener('storage', e => {
  if (e.key === tokenKey && e.newValue !== e.oldValue) {
    store.commit.auth('setToken', e.newValue);
    store.dispatch.auth('renew');
  }
});

let renewTimeout: NodeJS.Timer;

export const module: StoreModule<State, mutations, actions, getters> = {
  namespaced: true,
  state: {
    token: null,
    userData: null,
    lastRenewDate: new Date(),
  },
  mutations: {
    setToken: (state, token: string) => {
      state.token = token;
      state.lastRenewDate = new Date();
      state.userData = getUserData(token);
      if (token) {
        localStorage[tokenKey] = token;
      } else {
        delete localStorage[tokenKey];
        console.log(localStorage);

      }
    },
  },
  actions: {
    renew: (context, options = {}) => {
      const token = context.state.token;
      if (!token) {
        return Promise.reject(new Error('no token'));
      }
      const currentUserData = context.state.userData;
      const params = {
        database: options.database || currentUserData && currentUserData.l1ce.database,
        mode: options.mode || currentUserData && currentUserData.l1ce.mode,
      };
      return axios.get({
        url: '/api/v0/login',
        headers: {
          Accept: 'application/json',
        },
        params,
        errorReporting: 'nonblocking',
      }).then(response => {
        context.commit('setToken', response.data.token);
        const userData = context.state.userData;
        if (userData) {
          const token_remaining_seconds = userData.exp - userData.iat;
          clearTimeout(renewTimeout);
          renewTimeout = setTimeout(() => {
            context.dispatch('renew');
          }, token_remaining_seconds * .4 * 1000);
        }
      });
    },
    init: context => {
      const token = localStorage[tokenKey];
      if (!token) {
        // to break a possible login loop, we check if this flow was
        // initiated in the last 10 seconds using lastvisit_date
        // in session storage
        const currentMillis = (new Date()).getTime();
        if (sessionStorage.getItem(lastVisitKey)) {
          const timediff = currentMillis - Number(sessionStorage.getItem(lastVisitKey));
          if (timediff < 10000) {
            console.error('login loop detected, aborting');
            console.error('JWT token = ', localStorage.getItem(tokenKey));
            return;
          }
        }
        sessionStorage.setItem(lastVisitKey, currentMillis + '');
        window.location.href = '/api/v0/login';
        return Promise.reject(new Error('no auth information. redirected to CERN SSO'));
      } else {
        context.commit('setToken', token);
        return context.dispatch('renew')
        .catch(e => {
          console.error('error while initializing auth', e);
          context.commit('setToken', null);
          window.location.href = '/api/v0/login';
        });
      }
    },
    logout: context => {
      context.commit('setToken', null);
    },
  },
  getters: {
    isReadMode: s => s.userData ? s.userData.l1ce.mode !== 'write' : true,
    isAdmin: s => s.userData ? s.userData.l1ce.roles.includes('admin') : false,
  },
};


