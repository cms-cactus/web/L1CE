import { StoreModule } from './_types';
import { Socket } from 'socket.io';
// import * as Bluebird from 'bluebird';
import { Promise } from 'bluebird';

import { notify } from '../components/app/alerting';
import { store } from '.';

// declare global {
//   interface Window {
//     socket: Socket;
//   }
// }
declare function io(url: string): Socket;

export interface State {
  connectionState: connectionStates;
}
export type connectionStates = 'offline' | 'online' | 'error';
export type mutations = 'setStatus';
export type actions = 'socketConnect' | 'socketDisconnect' | 'socketAuth';
export type getters = '';

let socket: Socket | null = null;

export const module: StoreModule<State, mutations, actions, getters> = {
  namespaced: true,
  state: {
    connectionState: 'offline',
  },
  mutations: {
    setStatus: (state, connected: connectionStates) => {
      state.connectionState = connected;
    },
  },
  actions: {
    socketDisconnect: context => {
      console.log('actions.disconnect');
      if (socket) {
        console.log('deleting socket');
        socket.removeAllListeners();
        socket.disconnect(true);
      }
      context.commit('setStatus', 'offline');
      socket = null;
    },

    socketConnect: context => new Promise((ok, nok) => {
      console.log('actions.connect');
      if (context.state.connectionState === 'online') {
        return ok();
      } else if (socket) {
        context.dispatch('socketDisconnect');
      }
      socket = io(window.location.host);
      socket.on('connect', () => {
        console.log('socket.on connect');
        ok();
      });
      socket.on('disconnect', () => {
        console.log('socket.on disconnect');
        context.commit('setStatus', 'offline');
        console.log('offline', socket);
        if (socket) {
          context.dispatch('socketConnect');
        }
      });
    })
    .then(() => context.dispatch('socketAuth')),

    socketAuth: context => new Promise((ok, nok) => {
      console.log('actions.authenticate');
      if (!socket) {
        return nok(new Error('no socket'));
      }
      const token = localStorage.getItem('CERN Session');
      if (!token) {
        return nok(new Error('no token present'));
      }
      const destroy = (s: Socket) => {
        s.removeAllListeners('unauthorized');
        s.removeAllListeners('authenticated');
      };
      socket.on('unauthorized', ({message}) => {
        console.log('socket.on unauthorized');
        if (socket) {
          socket.disconnect();
        }
        context.commit('setStatus', 'error');
        destroy(socket as Socket);
        store.dispatch.auth('logout');
        notify({
          message: 'credentials not valid, please reload',
          type: 'error',
          timeout: 5000,
          actions: ['reload'],
        }).then(action => {
          store.dispatch.auth('init');
        });
        nok(new Error(message)); // object passed wasn't Error
      });
      socket.on('authenticated', () => {
        console.log('socket.on authenticated');
        context.commit('setStatus', 'online');
        destroy(socket as Socket);
        ok();
      });
      socket.emit('authentication', { token });
    }),
  },
  // getters: {

  // },
};


