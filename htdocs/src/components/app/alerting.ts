export interface AlertOptions {
  message: string;
  title?: string;
  type: 'info' | 'warning' | 'error' | 'success';
  blocking?: boolean;
  timeout?: number;
  actions?: string[];
}

export interface Alert extends AlertOptions {
  count: number;
  blocking: boolean;
  timeout: number;
  actions: string[];
  cb: (any) => void;
}

export interface InputOptions {
  message: string;
  // type: 'text';
  maxlength?: number;
  default: string;
  cancelable: boolean;
}

export interface Input extends InputOptions {
  cb: (any) => void;
}

let resolve: (any) => void;
export const mountMe = element => {
  resolve(element);
};
const getAlertManager = new Promise<any>(ok => {
  resolve = ok;
});

export const notify = (options: AlertOptions): Promise<string> => {
  return getAlertManager.then(m => m.showAlert(options));
};

export const askInput = (options: InputOptions): Promise<any> => {
  return getAlertManager.then(m => m.askInput(options));
};
