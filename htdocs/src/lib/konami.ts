import '../assets/konami.css';

const paths = {
  mp3: '/media/rawr.mp3',
  ogg: '/media/rawr.ogg',
  png: '/img/rawr.png',
};

const magicword = [
  'ArrowUp',
  'ArrowUp',
  'ArrowDown',
  'ArrowDown',
  'ArrowLeft',
  'ArrowRight',
  'ArrowLeft',
  'ArrowRight',
  'b',
  'a',
];

let pos = 0;

window.onload = function setupRaptor() {
  document.addEventListener('keydown', function raptorKeyDown(event) {
    if (event.key !== magicword[pos]) {
      pos = 0;
    } else {
      pos += 1;
      if (pos === magicword.length) {
        makeRaptor(true);
        pos = 0;
      } else if (pos === 2) {
        // start preload if we have a hunch the user
        // is about to say the magic word
        makeRaptor(false);
      }
    }
  });
};

function makeRaptor(forReal: boolean = true) {
  const raptorimg = document.createElement('img');
  raptorimg.setAttribute('id', 'rawr');
  raptorimg.setAttribute('src', paths.png);

  const raptorsound = document.createElement('audio');
  const raptorsoundsource1 = document.createElement('source');
  const raptorsoundsource2 = document.createElement('source');
  raptorsoundsource1.setAttribute('src', paths.mp3);
  raptorsoundsource1.setAttribute('type', 'audio/mpeg');
  raptorsoundsource2.setAttribute('src', paths.ogg);
  raptorsoundsource2.setAttribute('type', 'audio/ogg');
  raptorsound.appendChild(raptorsoundsource1);
  raptorsound.appendChild(raptorsoundsource2);

  if (forReal) {
    raptorimg.setAttribute('animate', 'animate');
    document.body.appendChild(raptorimg);
    document.body.appendChild(raptorsound);
    raptorsound.play();

    setTimeout(() => {
      document.body.removeChild(raptorimg);
      document.body.removeChild(raptorsound);
    }, 6000);
  }
}
