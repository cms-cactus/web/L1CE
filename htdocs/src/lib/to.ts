// https://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/

// Proper error handling for async/await:
// import to from './to';
// [user, err] = await to(UserModel.findById(1));
// if(!user) throw new CustomerError('No user found');

export default function <T>(p: PromiseLike<T>): PromiseLike<[T | null, Error | null]> {
  return p.then(
    r => [r, null],
    e => [null, e instanceof Error ? e : new Error(e)],
  );
}
