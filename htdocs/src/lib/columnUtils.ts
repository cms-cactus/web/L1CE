export type ColumnType = 'readonly' | 'text' | 'key' | 'version' | 'date' | 'clob' | 'invisible';

export const modeColumns = ['KEYNAME', 'L1_HLT_MODE', 'L1_TRG_MODE', 'CMS_RUN_MODE'];

export const invisibleColumns = ['VALID', 'MODE_VALID', 'NAMESPACE', 'ANCESTOR'];

export const metaColumns = [
  'ID',
  ...modeColumns,
  'VERSION',
  'AUTHOR',
  'CREATION_DATE',
  'CREATION_AUTHOR',
  'DESCRIPTION',
];

export const getMode = (input: {}, ...extraModes: string[]) => {
  const modes = modeColumns.concat(extraModes);
  const key = Object.keys(input).find(k => modes.includes(k));
  if (key) {
    return [key, input[key]];
  } else {
    return [];
  }
};

const desiredColumnOrder = [
  'VALID',
  ...metaColumns,
  ...invisibleColumns,
  'KEYNAME',
  'CONF',
  'AMC13',
  'MP7',
  'HW',
  'INFRA',
  'ALGO',
  'UGT_KEY',
  'UGT_RS_KEY',
  'UGMT_KEY',
  'UGMT_RS_KEY',
  'CALOL1_KEY',
  'CALOL1_RS_KEY',
  'CALOL2_KEY',
  'CALOL2_RS_KEY',
  'BMTF_KEY',
  'BMTF_RS_KEY',
  'EMTF_KEY',
  'EMTF_RS_KEY',
  'OMTF_KEY',
  'OMTF_RS_KEY',
  'TWINMUX_KEY',
  'TWINMUX_RS_KEY',
  'CPPF_KEY',
  'TCDS_KEY',
  'ECAL_KEY',
  'HCAL_KEY',
  'RPC_KEY',
  'DT_KEY',
  'CONFIG_SEQUENCE3',
].reduce( (r, item, i) => {
  if (!r[item]) { r[item] = i + 1; } // avoid 0
  return r;
}, {} as {[columnName: string]: number});

export const sortColumns = (columns: string[]): string[] => {
  return columns.sort((columnA, columnB) => {
    const [a, b] = [columnA, columnB].map(c => desiredColumnOrder[c] || Infinity);
    return a < b ? -1 : a > b ? 1 : 0;
  });
};
