import moment from 'moment';

export const dbFormat = 'DD-MMM-YY hh.mm.ss.SSSSSS a';
export const viewFormat = 'dd MMM Do YYYY, H:m Z';

export const parse = (s: string) => {
  return moment(s, dbFormat);
};

export const formatForDB = (m: moment.Moment) => {
  return m.format(dbFormat);
};

export const formatForView = (m: moment.Moment | string) => {
  if (typeof m === 'string') {
    m = parse(m);
  }
  return m.format(viewFormat);
};
