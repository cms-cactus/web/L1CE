import * as Diff from 'diff';
import { store } from '../store';
import { sortColumns, metaColumns } from './columnUtils';

export interface DiffRow {
  type: 'foreign-key' | 'invalidation' | 'clob' | 'date' | 'text';
  columnName: string;
  oldVal: string;
  newVal: string;
  diff?: Diff.Change[];
  refTable?: string;
}

export interface DiffData {
  headers: DiffRow[];
  meta: DiffRow[];
  data: DiffRow[];
}

export async function getDiff(tableName: string, key1: string, key2: string): Promise<DiffData> {
  const response1 = await store.dispatch.database('getRow', [tableName, key1]);
  const response2 = await store.dispatch.database('getRow', [tableName, key2]);
  const row1 = response1.data.row;
  const row2 = response2.data.row;

  const diffData = sortColumns(Object.keys(row1)).map<DiffRow>(columnName => {
    const oldVal = row1[columnName];
    const newVal = row2[columnName];
    const columnDef = response1.data.metaData.find(col => col.name === columnName);
    const type = getDiffType(columnDef);

    const diffRow: DiffRow = {
      columnName,
      oldVal,
      newVal,
      type,
    };
    if (type !== 'clob') {
      diffRow.diff = Diff.diffChars(oldVal, newVal);
    }
    if (type === 'foreign-key') {
      diffRow.refTable = response1.data.metaData.find(m => m.name === columnName)!.FOREIGN_TABLE_NAME;
    }
    return diffRow;
  }).reduce<DiffData>((r, diffRow) => {
    const s = diffRow.type === 'invalidation' ? r.headers :
      metaColumns.includes(diffRow.columnName) ? r.meta : r.data;
    s.push(diffRow);
    return r;
  }, {headers: [], meta: [], data: []});

  return diffData;
}

function getDiffType(columnDef): DiffRow['type'] {
  if ((columnDef.FOREIGN_TABLE_NAME) && (columnDef.name !== 'HLT_KEY')) {
      return 'foreign-key';
    } else if (columnDef.name === 'VALID') {
      return 'invalidation';
    } else if (columnDef.data_type.indexOf('CLOB') === 0) {
      return 'clob';
    } else if (columnDef.data_type.indexOf('TIMESTAMP') === 0) {
      return 'date';
    } else {
      return 'text';
    }
}
