// These values are defined at compile time
// in vue.config.js

declare const GIT_LATEST_TAG: string;
declare const GIT_TAG_TRAILING_COMMITS: number;
declare const GIT_HASH: string;
declare const GIT_BRANCH: string;
declare const GIT_UNCOMMITTED_FILES: boolean;

declare const BUILD_TIMESTAMP: string;
declare const BUILD_HOST: string;
declare const BUILD_FROM_CI: boolean;
declare const BUILD_UNAME: string;
declare const BUILD_KERNEL: string;
declare const BUILD_ARCH: string;

export default {
  git: {
    latest_tag: GIT_LATEST_TAG,
    trailing_commits: GIT_TAG_TRAILING_COMMITS,
    hash: GIT_HASH,
    branch: GIT_BRANCH,
    uncommitted_files: GIT_UNCOMMITTED_FILES,
  },
  build: {
    timestamp: BUILD_TIMESTAMP,
    host: BUILD_HOST,
    CI: BUILD_FROM_CI,
    uname: BUILD_UNAME,
    kernel: BUILD_KERNEL,
    arch: BUILD_ARCH,
  },
};
