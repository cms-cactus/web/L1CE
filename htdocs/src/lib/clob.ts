import { store } from '../store';
import { notify } from '../components/app/alerting';
import { CompiledRow } from '../components/editor/rowEditor.vue';

const hidden = document.createElement('div');
hidden.style.position = 'absolute';
hidden.style.top = '-1000px';
hidden.style.left = '-1000px';

function resetHidden() {
  while (hidden.firstChild) {
    hidden.removeChild(hidden.firstChild);
  }
  document.body.removeChild(hidden);
}

const handleFileUpload = (event: any) => new Promise<string>((ok, nok) => {
  console.log('handleFileUpload', event);
  if (!event.target) {
    ok('');
  }

  const fileList: FileList = event.target.files;
  if (fileList.length !== 1) {
    return;
  }
  const file = fileList[0];
  if (file.type !== 'text/plain' && file.type !== 'text/xml' && file.type !== 'application/json') {
    nok(`upload of file of type ${file.type || 'unknown'} is not allowed`);
  }
  const reader = new FileReader();
  reader.onload = e => {
    const target = e.target as FileReader;
    const fileContent = target.result;
    if (fileContent instanceof ArrayBuffer) {
      return notify({
        message: `expected string, received ArrayBuffer`,
        type: 'error',
        timeout: 10000,
      });
    }
    ok(fileContent || '');
  };
  reader.readAsBinaryString(file);
});

export function download(entry: CompiledRow) {
  if (!entry.table || !entry.key || !entry.columnName) {
    return;
  }
  const url = `/api/v0/db/tables/${entry.table}/${encodeURIComponent(entry.key)}/getclob`;


  const form = document.createElement('form');
  form.action = url;
  form.method = 'post';

  let input = document.createElement('input');
  input.type = 'hidden';
  input.name = 'token';
  input.value = store.state.auth.token || '';
  form.appendChild(input);

  input = document.createElement('input');
  input.type = 'hidden';
  input.name = 'download';
  input.value = 'true';
  form.appendChild(input);

  input = document.createElement('input');
  input.type = 'hidden';
  input.name = 'clobColumnName';
  input.value = entry.columnName;
  form.appendChild(input);

  const button = document.createElement('button');
  button.type = 'submit';
  form.appendChild(button);

  hidden.appendChild(form);
  document.body.appendChild(hidden);

  button.click();

  resetHidden();
}

export const upload = () => new Promise<string>(ok => {
  const input = document.createElement('input');
  input.type = 'file';
  input.addEventListener('change', event => {
    handleFileUpload(event)
    .catch(reason => notify({
      message: reason.toString(),
      type: 'error',
      timeout: 3000,
    }))
    .then(newContent => {
      resetHidden();
      ok(newContent);
    });
  });
  hidden.appendChild(input);
  document.body.appendChild(hidden);
  input.click();
});
