import Vue from 'vue';
import Router from 'vue-router';
import { store } from '@/store';
import { notify } from '../components/app/alerting';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: 'editor',
    },
    {
      path: '/changes',
      name: 'changes',
      component: () => import(/* webpackChunkName: "changes" */ '../views/Changes.vue'),
    },
    {
      path: '/workflows',
      name: 'workflows',
      component: () => import(/* webpackChunkName: "workflows" */ '../views/Workflows.vue'),
    },
    {
      path: '/editor*',
      name: 'editor',
      component: () => import(/* webpackChunkName: "editor" */ '../views/Editor.vue'),
    },
    {
      path: '/compare',
      name: 'compare',
      component: () => import(/* webpackChunkName: "compare" */ '../views/Compare.vue'),
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import(/* webpackChunkName: "admin" */ '../views/Admin.vue'),
    },
  ],
});

const lsKey = 'L1CE_last_editor_path';
router.beforeEach((to, from, next) => {
  if (to.name !== 'editor' && from.name === 'editor') {
    localStorage[lsKey] = from.fullPath;
  } else if (to.name === 'editor' && from.name !== 'editor'
          && to.fullPath === '/editor' && localStorage[lsKey]
          && localStorage[lsKey] !== '/editor') {
    return next({ path: localStorage[lsKey] });
  }
  next();
});

export default router;
