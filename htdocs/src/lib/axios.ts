import axios, { AxiosTransformer, AxiosRequestConfig } from 'axios';

import { store } from '../store';
import { notify, AlertOptions } from '../components/app/alerting';

interface RequestConfig extends AxiosRequestConfig {
  errorReporting?: 'none' | 'nonblocking' | 'blocking';
}

const instanceConfig = {
  timeout: 20000,
  // headers: {'X-Custom-Header': 'foobar'},
  transformRequest: [(data, headers) => {
    headers.Authorization = 'Bearer ' + store.state.auth.token;
    return data;
  }, ...axios.defaults.transformRequest as AxiosTransformer[]],
};

const instance = axios.create(instanceConfig);

const makeErrorReporter = (style: RequestConfig['errorReporting']) => (error): Promise<any> => {
  console.dir(error);

  style = style || 'blocking';

  if (style === 'none') {
    return Promise.reject(error);
  }

  const response = error.response;
  let msg: string = (error.message || error.toString()) + '\n' +
                    error.config.method.toUpperCase() + ' ' + error.config.url;
  if (response) {
    let data = response.data;
    if (typeof data === 'string' && data[1] === '{') {
      try {
        data = JSON.parse(data as string);
      } catch (e) {
        console.error('failed to parse JSON response', data);
      }
    }
    if (data.error) {
      msg += '\n' + data.error;
    } else if (data.message) {
      msg += '\n' + data.message;
    } else if (typeof data === 'string') {
      msg += '\n' + data;
    } else {
      msg += '\n' + JSON.stringify(data);
    }
  }

  const notifyConfig: AlertOptions = {
    message: msg,
    type: 'error',
    timeout: 10000,
    actions: ['retry', 'ignore'],
  };
  if (style === 'nonblocking') {
    notifyConfig.actions = ['ok'];
    notify(notifyConfig);
    return Promise.reject(error);
  } else {
    return notify({
      message: msg,
      type: 'error',
      timeout: 10000,
      actions: ['retry', 'ignore'],
    }).then(action => {
      if (!action || action !== 'retry') {
        console.log('user didn\'t respond');
        return Promise.reject(error);
      } else {
        console.log('user response', action);
        return instance.request(error.config);
      }
    });
  }
};

const getConfig = (config: RequestConfig | string): RequestConfig => {
  return typeof config === 'string' ? {url: config} : config;
};

const handler = (config: RequestConfig | string) => {
  const c = getConfig(config);
  return instance(c).catch(makeErrorReporter(c.errorReporting));
};
(window as any).axios = handler; // for live debugging

const withMethod = (method: 'get' | 'put' | 'post' | 'delete' | 'options', config: RequestConfig | string) => {
  const c = getConfig(config);
  c.method = method;
  return handler(c);
};

handler.get = (config: RequestConfig | string) => withMethod('get', config);
handler.put = (config: RequestConfig | string) => withMethod('put', config);
handler.post = (config: RequestConfig | string) => withMethod('post', config);
handler.del = (config: RequestConfig | string) => withMethod('delete', config);
handler.options = (config: RequestConfig | string) => withMethod('options', config);

export default handler;
