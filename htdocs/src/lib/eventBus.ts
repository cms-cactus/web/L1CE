import Vue from 'vue';

export interface KeySavedEvent {
  table: string;
  newKey: string;
  oldKey: string;
  tableOwner: string;

  // for automatic recursive queuing
  description: string;
  autoQueueLevels: number;
}

interface EventMap {
  'key-saved': KeySavedEvent;
}

const eventBus = new Vue();

function on<K extends keyof EventMap>(type: K, handler: (event: EventMap[K]) => void): void {
  eventBus.$on(type, handler);
}

function off<K extends keyof EventMap>(type: K, handler: (event: EventMap[K]) => void): void {
  eventBus.$off(type, handler);
}

function emit<K extends keyof EventMap>(type: K, data: EventMap[K]) {
  eventBus.$emit(type, data);
}

export default {on, off, emit};
