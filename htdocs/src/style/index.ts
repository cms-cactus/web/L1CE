// setup global stylesheet
// also see vue.config.js

import './reset.scss';

// vue-material
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/src/theme/prebuilt/default.scss';
import './component_overrides/index.scss';
import './globals.scss';

// icons & fonts
import 'roboto-fontface/css/roboto/roboto-fontface.css';

// makes syntax highlighting in diff2html work
import './highlight.js/styles/github.min.css';
