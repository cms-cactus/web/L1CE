# Level-1 Configuration Editor [![build status](https://gitlab.cern.ch/cms-cactus/web/L1CE/badges/master/build.svg)](https://gitlab.cern.ch/cms-cactus/web/L1CE/commits/master)

This is the code of the L1CE, an integrated tool to assist in the creation and management of configuration keys for subsystems in the CMS experiment.

## [compilation guidelines](docs/compilation-guide.md)
## [installation guidelines](docs/installation-guide.md)
## [operator guidelines](docs/operator-guide.md)
## [development guidelines](CONTRIBUTING.md)
