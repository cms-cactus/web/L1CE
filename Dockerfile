FROM cern/cc7-base:20210601-1

# https://github.com/opencontainers/image-spec/blob/master/annotations.md#pre-defined-annotation-keys
LABEL org.opencontainers.image.description="Level-1 Configuration Editor"
LABEL org.opencontainers.image.authors="Cactus <cactus@cern.ch>"

ADD http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/Packages/oracle-instantclient19.3-basic-19.3.0.0.0-2.x86_64.rpm /rpms/
ADD http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/Packages/oracle-instantclient19.3-devel-19.3.0.0.0-1.x86_64.rpm /rpms/
ADD http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/Packages/oracle-instantclient19.3-sqlplus-19.3.0.0.0-1.x86_64.rpm /rpms/
ADD http://service-oracle-tnsnames.web.cern.ch/service-oracle-tnsnames/tnsnames.ora /etc/tnsnames.ora

ADD L1CE-*cc7.x86_64.rpm /rpms/
RUN yum install -y /rpms/*.rpm openssl && \
    yum clean all && \
    adduser l1ce
USER l1ce

CMD /opt/cactus/L1CE/node/bin/node /opt/cactus/L1CE/api/main.js