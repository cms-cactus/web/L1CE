# Installing L1CE using an RPM package

Every tagged release of the code is accompanied by 'build artifacts', a zip file containing RPMs for L1CE and all dependencies.

The goal of this set of RPMs is to provide everything necessary to get the latest L1CE up and running on a fresh install of CC7-base with no internet access.

The latest commit of the [release branch](https://gitlab.cern.ch/cms-cactus/web/L1CE/commits/release) should always have a tag, and therefore a build.

Please note: you need also to perform all the steps foreseen at the end of this guide if you want to run a custom instance in a private host.

## Getting the latest RPM

First, a set of prerequisites must be accomplished: [installation prerequisites](install-prerequisites.md). Then the following installation steps:

1. Go to [the L1CE repository](https://gitlab.cern.ch/cms-cactus/web/L1CE)

   ![](install-rpm-1.png)

1. Click on `commits`, it will redirect you to [this page](https://gitlab.cern.ch/cms-cactus/web/L1CE/commits/master)

   ![](install-rpm-2.png)

1. In the dropdown, select the latest tag version. At the time of writing this is version `0.3.0`

   ![](install-rpm-3.png)

1. Click the green checkmark next to the latest commit

   ![](install-rpm-4.png)

1. Click `builds`

   ![](install-rpm-5.png)

1. Click the `build` button

   ![](install-rpm-6.png)

1. Click `Download` in the Build artifacts section

   ![](install-rpm-7.png)


## Installing the RPMs

On your target system, execute

```bash
sudo yum install L1CE-*.rpm
```

This should install or upgrade L1CE, start it, and set it up to start on boot.

## Extra configuration steps for custom host

In order to run a custom instance in a private host, extra configuration steps are needed: [configuration](configuration.md).


## Additional debug hints

In case of authentication problems check if `nginx` is down or there are errors in `/var/log/nginx/error.log`.
Other errors are logged in the l1ce lofile `/var/log/l1ce/error.log`

For details on how to operate the l1ce, have a look at the [operator guide](operator-guide.md)
