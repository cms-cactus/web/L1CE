# How to compile

## directories description

- `api` : contains the server source files
- `configs` : contains host configuratiuon files
- `docs` : contains documentation
- `errors` : contains html errore responses
- `htdocs` : contains interface source files

## first installation

First, a set of prerequisites must be accomplished: [installation prerequisites](install-prerequisites.md).
Right after, the compilation and installation steps are performed through:

- `make build` to compile
- `make rpm` to create RPMs that can be installed via YUM. Use RPMs from GitLab CI when installing at P5.

In order to run a custom instance in a private host, extra configuration steps are needed: [configuration](configuration.md).

## Targets description

Run `make help` to see a list of all possible targets with the relative explanation.