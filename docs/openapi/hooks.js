const request = require('request');

let authKey = "";
let createdKey = ";"

module.exports = {
  beforeServer: (openAPIDef, serverUrl, cb) => {    
    request({
      url: serverUrl + "/login/bot",
      json: true
    }, (error, response, body) => {        
      if (error) {
        cb(error);
      } else {
        authKey = body.token;
        cb();
      }
    });
  },

  beforeEach: (requestOpts, cb) => {
    requestOpts.headers.Authorization = `JWT ${authKey}`;
    cb();
  },

  beforeOperation: {
    commitDBChanges: (requestOpts, methodDef, cb) => {
      methodDef.responses['403'] = methodDef.responses['200'];
      delete methodDef.responses['200'];
      cb();
    },
    login: (requestOpts, methodDef, cb) => {
      methodDef.responses['200'] = {
        content: {
          'text/html': {}
        }
      }
      methodDef.responses['403'] = {
        content: {
          'application/json': {}
        }
      }
      cb();
    },
    validateKey: (requestOpts, methodDef, cb) => {
      requestOpts.url.pathname = '/api/v0/db/tables/BMTF_KEYNAMES/DUMMY/validate';
      cb();
    },
    invalidateKey: (requestOpts, methodDef, cb) => {
      requestOpts.url.pathname = '/api/v0/db/tables/BMTF_KEYNAMES/DUMMY/invalidate';
      cb();
    },
    getclob: (requestOpts, methodDef, cb) => {
      requestOpts.url.pathname = requestOpts.url.pathname.replace('DUMMY', createdKey);
      cb();
    }
  },

  afterOperation: {
    createTableRow: (requestOpts, methodDef, response, body, cb) => {
      createdKey = JSON.parse(body).row.ID;
      cb();
    }
  },
}
