#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'
cd `dirname $0`

if which python3 &> /dev/null; then
  python3 -m http.server
else
  python -m SimpleHTTPServer 8000
fi
