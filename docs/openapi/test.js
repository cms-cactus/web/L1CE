#!/usr/bin/env node
'use strict'

const commander = require('commander');
const request = require('request');
const requireYml = require('require-yml');
const term = require('terminal-kit').terminal;
const fs = require('fs');
const os = require('os');
const { URL } = require('url');
const ajv = new require('ajv')({ schemaId: 'auto' });
ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

commander
  .arguments('<filepath>')
  .option('-s,--server [server]', 'override the server base URL')
  .parse(process.argv);
const openAPIFile = commander.args[0] || 'l1ce.yaml';

if (!openAPIFile) {
  throw new Error("OpenAPI File path not given");
}
const apiDef = requireYml(openAPIFile);
if (commander.server) {
  apiDef.servers = [{ url: commander.server }];
} else if (!apiDef.servers) {
  apiDef.servers = [{ url: '/' }];
}

if (apiDef.openapi[0] !== '3') {
  console.error("need OpenAPI 3 document, got", apiDef.openapi);
  throw new Error("wrong openapi version");
}

const hooks = {
  beforeServer: (apiDef, server, cb) => cb(),
  beforeEach: cb => cb(),
  beforeOperation: {
    // exampleOperationId: (requestOpts, methodDef, cb) => cb()
  },
  afterOperation: {
    // exampleOperationId: (requestOpts, methodDef, response, body, cb) => cb()
  },
  skipOperations: []
};
try {
  const customhooks = require('./hooks');
  Object.assign(hooks, customhooks);
} catch (error) {
  console.warn("no hooks.js applied", error.message);
}

let testFailed = false;

function testAll() {
  console.log(apiDef.info.title + ":");
  let p = Promise.resolve();
  apiDef.servers.forEach(({ url }) => {
    console.log(url);
    hooks.beforeServer(apiDef, url, err => {
      if (err) {
        handleEnd(Promise.reject(err));
      } else {
        Object.keys(apiDef.paths).forEach(path => {
          p = p.then(() => testPath(url, path));
        });
        handleEnd(p);
      }
    });
  });
}
testAll();

function handleEnd(p) {
  p.catch(err => {
    console.error("an error occurred", err);
    testFailed = true;
  }).then(() => {
    process.exit(testFailed ? 1 : 0);
  });
}

function testPath(server, path) {
  console.log("  " + path);
  const pathDef = apiDef.paths[path];
  let p = Promise.resolve();
  Object.keys(pathDef).forEach(httpMethod => {
    const methodDef = pathDef[httpMethod];
    if (hooks.skipOperations.indexOf(methodDef.operationId) === -1) {
      p = p.then(() => testMethod(server, path, methodDef, httpMethod));
    }
  });
  return p;
}

function printStatus(message, status = '?') {
  let colorname = 'defaultColor';
  if (status === '?') {
    colorname = 'blue';
  } else if (status === 'v') {
    colorname = 'green';
  } else if (status === 'x') {
    colorname = 'red';
  }

  term('[')[colorname](status)('] ')[colorname](message)(os.EOL);
}

function testMethod(server, path, methodDef, method) {
  // const methodDef = pathDef[method];

  let url = new URL(server);
  let realPath = url.pathname + path;
  if (methodDef.parameters) {
    methodDef.parameters.forEach(param => {
      if (param.in === 'path') {
        if (!param.example) {
          term.red("path parameter has no example value to use");
          console.log({ param });
          testFailed = true;
          return Promise.resolve();
        } else {
          realPath = realPath.replace(`{${param.name}}`, param.example);
        }
      } else if (param.in === 'query') {
        if (param.required && !param.example) {
          term.red("required query parameter has no example value to use");
          console.log({ param });
          testFailed = true;
          return Promise.resolve();
        } else {
          url.searchParams.append(param.name, param.example);
        }
      } else {
        term.red("unknown parameter type " + param.in);
        console.log({param});
      }
    });
  }
  url.pathname = realPath;
  const methodMsg = `${methodDef.operationId}: ${method} ${url.pathname}${url.search}`;

  const requestOpts = {
    method,
    url,
    headers: {},
    time: true
  }
  
  if (methodDef.requestBody) {
    const bodyDef = methodDef.requestBody;
    const contentType = Object.keys(bodyDef.content)[0];
    const body = bodyDef.content[contentType].example;
    if (!body && bodyDef.required) {
      term.red("required request body has no example" + os.EOS);
      testFailed = true;
    } else if (body) {
      if (body.$ref) {
        requestOpts.body = fs.createReadStream(body.$ref);
      } else {
        requestOpts.body = body;
      }
      requestOpts.headers['Content-type'] = contentType;
    }
  }

  return new Promise(ok => {
    hooks.beforeEach(requestOpts, err => {
      if (err) {
        term.red("error in beforeEach");
        console.log(err);
        testFailed = true;
      }
      ok();
    });
  }).then(() => new Promise(ok => {
    const hook = hooks.beforeOperation[methodDef.operationId];
    if(hook) {
      hook(requestOpts, methodDef, err => {
        if (err) {
          term.red("error in beforeOperation hook for" + methodDef.operationId);
          console.log(err);
          testFailed = true;
        }
        ok();
      });
    } else {
      ok();
    }
  })).then(() => new Promise((ok, nok) => {    
    printStatus(methodDef.operationId);
    request(requestOpts, (error, response, body) => {
      if (error) {
        console.error("error:", error);
        testFailed = true;
      } else {
        const httpCode = response.statusCode;
        const contentType = response.headers['content-type'];
        const responseDef = methodDef.responses[httpCode];
        const timings = response.timings;
        if (!responseDef) {
          // term.previousLine(1);
          printStatus(methodMsg, 'x');
          term.red("unexpected response code");
          console.error({ httpCode, body });
          testFailed = true;
          return ok();
        } else if (!responseDef.content[contentType]) {
          // term.previousLine(1);
          printStatus(methodMsg, 'x');
          term.red("unexpected response content type " + contentType + " (expected: " + Object.keys(responseDef.content).join(', ') + ')');
          console.error({httpCode, body});
          testFailed = true;
          return ok();
        } else if (!responseDef.content[contentType].schema) {
          // term.previousLine(1);
          printStatus(methodMsg, 'v');
          term.yellow("no schema for response, not validating");
          return ok(1);
        }

        const schemaDef = responseDef.content[contentType].schema;
        new Promise((ok, nok) => {
          const hook = hooks.afterOperation[methodDef.operationId];
          if (!hook) {
            ok();
          } else {
            hook(requestOpts, methodDef, response, body, err => {
              if (err) {
                term.red("error in afterOperation hook for" + methodDef.operationId);
                console.log(err);
                testFailed = true;
                nok();
              }
              ok();
            });
          }
        }).then(() => new Promise((ok, nok) => {
          if (!schemaDef.$ref) {
            ok(schemaDef);
          } else {
            fs.readFile(schemaDef.$ref, { encoding: 'utf8' }, (err, data) => {
              err ? nok(err) : ok(JSON.parse(data));
            });
          }
        })).then(schema => {
          if (contentType !== "application/json") {
            // term.previousLine(1);
            printStatus(methodMsg, 'v');
            term.yellow("can't validate schema for this content-type");
            console.error({contentType});
            return ok(1);
          } else {
            const validator = ajv.compile(schema);
            if (validator(JSON.parse(body))) {
              // term.previousLine(1);
              printStatus(methodMsg, 'v');
              return ok(1);
            } else {
              // term.previousLine(1);
              printStatus(methodMsg, 'x');
              console.log("validation failed", body);
              console.error(ajv.errorsText());
              testFailed = true;
            }
          }
          return ok(1);
        }).catch(err => {
          term.red("failed to fetch schema");
          console.error(err);
          testFailed = true;
          return ok();
        });
      }
    });
  }));
}
