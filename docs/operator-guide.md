# L1CE operator guide

## Starting L1CE server

```bash
sudo systemctl start l1ce
```

## Stopping L1CE server

```bash
sudo systemctl stop l1ce
```

## Restarting L1CE

```bash
sudo systemctl restart l1ce
```