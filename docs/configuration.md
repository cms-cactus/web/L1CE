# Configuration

Configuration files are located in the `configs` folder.

In the installed version of the L1CE (e.g. installed through rpm), this folder is located here: `/opt/cactus/L1CE/configs`.

The codebase detects the hostname at P5 to run with the correct conf (`l1ce.cms.yaml` or `l1ce-test.cms.yaml`).
Outside P5 configuration can be passed via the ```--config-file``` option, e.g.:

```bash
node api/main.js --config-file /absolute/path/to/conf.yaml 
```

## Running on a private instance

`configs/development.yaml` contains a template of the configuration required to run on a private instance.
Set the `client_id` to the OAuth Client ID obtained from the CERN Application portal (see [prerequisites](install-prerequisites.md)) and `secret_path` to a path leading to a file containing the OAuth Secret (again, see [prerequisites](install-prerequisites.md)).