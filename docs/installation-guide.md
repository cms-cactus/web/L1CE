# Installation guide

This guide assumes you want to start from zero (just the source).

Alternatively, you can [install from pre-built RPMs](install-rpm.md).

In both cases, the basic installation is preconfigured for P5 deployment, and will require extra configuration steps.

First, a set of prerequisites must be accomplished: [installation prerequisites](install-prerequisites.md).

## Getting the code

This guide assumes you have `git` installed.

```bash
git clone https://gitlab.cern.ch/cms-cactus/web/L1CE.git
```

Now you have the latest version of the code. If you want to have a specific (or the latest) release, run

```bash
git tag -l
```
This will give you a list of tags, after which you can get the code of a particular release (e.g. v0.3.0) like this:

```bash
git checkout tags/0.3.0
```


## Installing from source

Reading `.gitlab-ci.yml` will give you a live example of the build instruction. They are used to build the test environment and build the RPMs.
The docker image built by ```.gitlab/ci/builder.Dockerfile``` can give a reference of the environment and dependencies required to build and run L1CE.

### Build from Docker

The easiest way to build and run L1CE is in the Docker image used by the CI image.

Run ```make help``` for a full description of the make targets.


## Common mistakes

*  prerequisites not fully accomplished (dependencies missing, SELinux of firewall enabled, ... )
*  host not registered
*  missing keypairs
*  the key pair path and names are no correctly spelled (you will see errors both in the L1CE and nginx log files)
*  the hostname is not properly spelled


## How to setup the P5 test instace

Download the `artifact.zip` from the tag page and copy it to `cmsusr`.
Then login to the `l1ce-test.cms` pc and unzip the package:

```bash
unzip artifact.zip
```

make sure you have sudo rights, then:

```bash
sudo systemctl stop l1ce
sudo yum remove L1CE
sudo yum install /path/to/l1ce/rpm/L1CE-*.x86_64.rpm
sudo systemctl start l1ce
```

## How to install at P5

DROPBOX INSTRUCTIONS GO HERE.