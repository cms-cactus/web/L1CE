# Contributing

![architecture](docs/development flow.png)

## Issue tracking system
The [issue tracking system](https://gitlab.cern.ch/cms-cactus/web/L1CE/issues) is the first point of entry. There you can see what work needs to be done, or you can submit a new issue representing a problem or new work.

## Issue board
The issue board also contains all open issues, but here they are categorised in columns based on their status.

If you are familiar with SCRUM this board will be familiar to you.

## Branches workflow
This is a derived version from [Gitlab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/), which itself is derived from [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

### Master branch
The master branch represents the latest version of the software that is considered stable.

All feature development should take place in a dedicated branch instead of the master branch. In fact, any commit directly to the master branch will be rejected.

### Development branches
Any addition to the master branch is done by merging other branches into the master branch, by means of a pull request.
This encapsulation makes it easy for multiple developers to work on a particular feature without disturbing the main codebase.

This allows for code review by authorized parties before code is added to the master branch.

Encapsulating feature development also makes it possible to leverage pull requests, which are a way to initiate discussions around a branch. They give other developers the opportunity to sign off on a feature before it gets integrated into the official project. Or, if you get stuck in the middle of a feature, you can open a pull request asking for suggestions from your colleagues.

Development branches should have descriptive names, and must be prefixed by a ticket number from the issue tracking system.

### Release branch
When it is time for a new release, the current state of the master branch is merged into the release branch by means of a pull request.

After the merge, apply a tag to the merge commit. This will trigger the build process.
After that, all RPMs needed for deployment are available for download as build artifacts.

This action represents a feature freeze. Only critical bugs can be added at this point using hotfix branches.

### Hotfix branches
When a release contains a problem that needs to be fixed urgently, you can create a branch based on the release branch.
They do not require a pull request to be merged into the release branch.

This allows you to quickly patch any bugs without too much ceremony.

Hotfix branches must be merged back into the release AND master branch.
Note that only a master can perform these merges.
