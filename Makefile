SHELL:=/bin/bash
.DEFAULT_GOAL := help

include .makefile/vars.mk

##
### Main commands
##

.PHONY: build
build: node api htdocs ## build all code

.PHONY: api
api: ## build API
	$(MAKE) -C api --no-print-directory build

.PHONY: htdocs
htdocs: ## build web UI
	$(MAKE) -C htdocs --no-print-directory build

.PHONY: docs
docs: ## build documentation
	$(MAKE) -C docs --no-print-directory build

.PHONY: rpm
rpm: rpm.cc7 rpm.c8 ## build all RPMs
rpm.%: ## build RPM for cc7 or c8
	$(MAKE) --no-print-directory L1CE-${VERSION}.$*.x86_64.rpm

.PHONY: clean
clean: ## clean up
	$(MAKE) -C api --no-print-directory clean
	$(MAKE) -C htdocs --no-print-directory clean
	$(MAKE) -C docs --no-print-directory clean
	rm -f *.rpm

.PHONY: cleanall
cleanall: clean ## full cleanup
	$(MAKE) -C api --no-print-directory cleanall
	$(MAKE) -C htdocs --no-print-directory cleanall
	$(MAKE) -C docs --no-print-directory cleanall

##
### Helper commands
##

version: ## display version string for this repo
	@echo ${VERSION}

node: # for stability at P5, we pack the node binary with our RPMs
	curl -Lo /tmp/node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-${uname_s}-${NODE_ARCH}.tar.xz
	tar -xJvf /tmp/node.tar.xz -C . >/dev/null
	mv node-v${NODE_VERSION}-${uname_s}-${NODE_ARCH} node
	rm -rf /tmp/node.tar.xz

.ONESHELL:
L1CE-${VERSION}.%.x86_64.rpm:
	$(MAKE) --no-print-directory build
	[[ $@ == *c8* ]] && maybelibnsl="-dlibnsl"
	fpm \
	-p $@ \
	-s dir \
	-t rpm \
	-n L1CE \
	-d libaio -d iproute -d bind-utils $$maybelibnsl \
	-d oracle-instantclient19.3-basic -d oracle-instantclient19.3-devel \
	-m "<cactus@cern.ch>" \
	-v ${CI_COMMIT_TAG} \
	--iteration ${COMMITS_SINCE_LAST_TAG} \
	--vendor CERN \
	--description "Level-1 Configuration Editor" \
	--url "https://gitlab.cern.ch/cms-cactus/web/L1CE" \
	--provides l1ce \
	api=/opt/cactus/L1CE \
	htdocs/dist=/opt/cactus/L1CE/htdocs \
	configs=/opt/cactus/L1CE \
	node=/opt/cactus/L1CE \
	systemd/=/usr/lib/systemd/system

include .makefile/dockerized.mk
include .makefile/help.mk