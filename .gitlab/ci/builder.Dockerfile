FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-cc7-gcc8:tag-0.2.3
LABEL org.opencontainers.image.authors="Cactus <cactus@cern.ch>"

ADD http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/Packages/oracle-instantclient19.3-basic-19.3.0.0.0-2.x86_64.rpm /rpms/
ADD http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/Packages/oracle-instantclient19.3-devel-19.3.0.0.0-1.x86_64.rpm /rpms/
ADD http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64/Packages/oracle-instantclient19.3-sqlplus-19.3.0.0.0-1.x86_64.rpm /rpms/
ADD http://service-oracle-tnsnames.web.cern.ch/service-oracle-tnsnames/tnsnames.ora /etc/tnsnames.ora

RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -
RUN yum install -y nodejs /rpms/*.rpm openssl && yum clean all