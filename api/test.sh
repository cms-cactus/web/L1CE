#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail -x
IFS=$'\n\t'

cd `dirname $0`

node main.js &

sleep 10
ps -p $! &> /dev/null # this will fail if server initialization failed

cd ../docs/openapi
npm i
! ./test.js l1ce.yaml --server https://localhost/api/v0
exitcode=${PIPESTATUS[0]}

kill %1
sleep 3
exit $exitcode
