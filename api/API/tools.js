var fs = require('fs');
var APIRoot = "";

var getAPIRoot = exports.getAPIRoot = (dirname) => {
  return global.prefix + dirname.substring(APIRoot.length);
}

var initAPIRoot = exports.initAPIRoot = (dirname) => {
  APIRoot = dirname + "/api";
}
