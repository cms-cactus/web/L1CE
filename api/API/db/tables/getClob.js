module.exports = ({connection, tableName, primaryColumn, primaryKey, clobColumnName}) => new Promise( (ok, mitä_vittua) => {
  if (!(connection && tableName && primaryColumn && primaryKey && clobColumnName)) {
    console.error("getClob - not enough parameters", !!connection, tableName, primaryColumn, primaryKey, clobColumnName);
    mitä_vittua("not enough parameters");
  } else {
    connection.execute(
      // thank you shitty data binding
      `select ${clobColumnName} from ${tableName} where ${primaryColumn} = :primaryKey`,
      {
        primaryKey: primaryKey
      }
    )
    .then( result => {
      var lob = result.rows[0][clobColumnName];
      if (!lob) {
        ok("");
      }
      var clob = "";
      // makes the lob a string instead of a buffer
      lob.setEncoding('utf8');
      lob.on('data', chunk => clob += chunk);
      lob.on('end', () => ok(clob));
      lob.on('error', error => mitä_vittua(`Error while fetching clob: ${error}`));
    })
    .catch( error => {
      mitä_vittua(`Error while getting clob: ${error}`);
    });
  }
});
