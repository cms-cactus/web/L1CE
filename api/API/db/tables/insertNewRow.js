var setClob = require("./setClob.js");
var oracledb = require('oracledb');
var { io_promise } = require.main.require("./makeWebSocket.js");
var moment = require('moment');
var getTable = require("./getTable.js").getTable;
var db = require.main.require('./database.js');

module.exports = ({
    tableName,
    row: newRow,
    sourceKey,
    credentials
  }) => new Promise( (yep, nope) => {
    // get the current rows
    getTable({ tableName, credentials })
    // modify newRow data
    // latest version, author, creation_date
    .then( ({table, owner, primaryColumn}) => {
      /*
       * check the list of pending changes
       * changes over multiple db users are not allowed
       */
      var userIsDifferent = db.databaseChanges[credentials.l1ce.database][credentials.cern.fullname].some( change => change.tableOwner != table.owner );
      if (userIsDifferent) {
        throw new Error("User has pending changes that need to be committed first");
      }

      var mode_column = table.metaData.find( c => c.name == "KEYNAME" || c.name == "L1_HLT_MODE" || c.name == "L1_TRG_MODE" || c.name == "CMS_RUN_MODE");
      var has_nameSpace = table.metaData.some( c => c.name == "NAMESPACE");
      if (!mode_column) {
        mode_column = table.metaData.find( c => c.name == "NAMESPACE" );
      }
      var hasVersionColumn = !!table.metaData.find( c => c.name == "VERSION");
      var hasAncestorColumn = !!table.metaData.find( c => c.name == "ANCESTOR");

      /*
       * these values must be ignored from the client, as they are generated
       * some of these columns may not exist, but we make the INSERT statement
       * based on the metaData anyway
       */
       // author is always the current user
      if ( table.metaData.find( c => c.name == "CREATION_AUTHOR" ) ) {
        newRow.CREATION_AUTHOR = credentials.cern.fullname;
      } else if ( table.metaData.find( c => c.name == "AUTHOR" ) ) {
        newRow.AUTHOR = credentials.cern.fullname;
      }
       // creation date is now, like "25-JUL-16 09.16.32.661868 AM"
      if ( table.metaData.find( c => c.name == "CREATION_DATE" ) ) {
        newRow.CREATION_DATE = moment.utc().format("D-MMM-YY hh.mm.ss.SSSSSS A");
      }
       // validation, new keys are always valid
      if ( table.metaData.find( c => c.name == "VALID" ) ) {
        newRow.VALID = "Y";
      }
      // version is always the newest increment of the mode
      var getMode = function(row) {
        var mode = row[mode_column.name];
        if (has_nameSpace) {
          // combine namespace and mode, but remove duplicates in the merge
          let namespace = row.NAMESPACE.toLowerCase();
          return namespace + "_" + mode.replace(namespace+"_", '');
        }
        else {
          return row[mode_column.name]
        }
      }
      if (hasVersionColumn) {
        // find the latest version of each mode
        let mode = mode_column ? getMode(newRow).toLowerCase() : "";
        let version = table.rows
        .map( row => ({
          mode: mode_column ? getMode(row).toLowerCase() : undefined,
          version: Number(row.VERSION)
        }))
        .filter( row => row.mode ? row.mode == mode : true)
        .reduce( (result, row) => result < row.version ? row.version: result, 0)
        let newversion = version+1;
        console.debug(`new version ${newversion} for mode ${mode}`);
        newRow.VERSION = `${newversion}`;

        let subsystem = tableName.split("_")
          .map( piece => piece.toLowerCase() )
          .filter( piece => mode.indexOf(piece+"_") == -1 )
          .reduce((a,b) => {
            if (b != "conf" && b != "keys" && b != "clobs") {
              return `${a}_${b}`;
            } else {
              return a;
            }
          });
        newRow[primaryColumn] = mode_column ? `${subsystem}_${mode}/v${newversion}` : `${subsystem}/v${newversion}`;
      }
      else if (mode_column) {
        newRow[primaryColumn] = getMode(newRow);
      }
      else {
        // we're not overriding the ID, check for valid input
        var invalid = !/^[A-Za-z0-9-_ \/]*$/.exec(newRow[primaryColumn])
        if (invalid) {
          throw {
            code: 400,
            message: `${primaryColumn} "${newRow[primaryColumn]}" contains invalid characters`
          }
        }
      }

      if ( hasAncestorColumn ) {
        newRow.ANCESTOR = decodeURIComponent(sourceKey);
      }

      return {
        table: table,
        owner: owner,
        primaryColumn: primaryColumn
      }
    })
    // construct query
    .then( ({table, owner, primaryColumn}) => {
      /*
       * go over all columns expected by the database
       * and construct queries (data+clobs)
       */
      var [clobjobs, arrColumnNames, arrValues, options] = table.metaData
      .filter( c => c.name != "CREATION_DATE")
      .reduce( ([clobjobs, arrColumnNames, arrValues, options], column) => {
        var columnName = column.name;
        var value = newRow[columnName];

        // if column is clob, make a new job to update it after insertion
        if (column.fetchType == oracledb.CLOB) {
          value && clobjobs.push({
            clobColumnName: columnName,
            clob: value
          })
        }
        // ignore VALID column if it is not needed
        else if ( (columnName == "VALID" && table.validation != "manual") || columnName == "MODE_VALID" ) {
        }
        // insert column name & value into query
        else {
          arrColumnNames.push(`${columnName}`);
          arrValues.push(`:${columnName}`);
          options[columnName] = {
            val: value,
            type: column.fetchType
          }
        }
        return [clobjobs, arrColumnNames, arrValues, options];
      }, [[], [], [], {}])

      return Promise.all([
        db.getConnection({ credentials, databaseUser: owner }),
        { arrColumnNames, arrValues, options },
        { clobjobs, primaryColumn }
      ])
    })
    .then( ([connection, {arrColumnNames, arrValues, options}, clobData ]) => {
      console.debug("inserting new key", `INSERT INTO "${tableName}" (${arrColumnNames.join(", ")}) VALUES (${arrValues.join(", ")})`, options);
      return Promise.all([
        connection.execute(`INSERT INTO "${tableName}" (${arrColumnNames.join(", ")}) VALUES (${arrValues.join(", ")})`, options),
        connection,
        clobData
      ])
    })
    .then( ([,connection, {clobjobs, primaryColumn}]) => {
      console.debug("row inserted, inserting clobs");
      return Promise.all([
        Promise.all(clobjobs.map( job => setClob(connection, `"${tableName}"`, primaryColumn, newRow[primaryColumn], job.clobColumnName, job.clob) )),
        primaryColumn,
        connection
      ])
    })
    .then( ([, primaryColumn, connection]) => {
      console.debug("inserted new row", {
        tableName,
        primaryKey: newRow[primaryColumn],
        sourcePrimaryKey: sourceKey
      });
      return io_promise.then(io => io.notifyNewTable({
        tableName,
        tableOwner: connection.userName,
        primaryKey: newRow[primaryColumn],
        sourcePrimaryKey: sourceKey,
        credentials
      }))
        .then(() => primaryColumn);
    })
    .then( primaryColumn => yep({newRow: newRow, primaryColumn: primaryColumn}) )
    .catch( error => {
      const msg = error.message || error;      
      if (msg.indexOf("ORA-00001") != -1) {
        nope({
          code: 409,
          message: "this key already exists"
        })
      }
      else {
        nope(`Error while inserting new row: ${msg}`)
      }
    });
});
