module.exports = (connection, tablename) => connection.streamExecute(
`SELECT column_name,
        data_type,
        data_type_mod,
        data_type_owner,
        data_length,
        data_precision,
        data_scale,
        nullable,
        column_id,
        default_length,
        num_distinct,
        density,
        num_nulls,
        num_buckets,
        last_analyzed,
        sample_size,
        character_set_name,
        char_col_decl_length,
        global_stats,
        user_stats
 FROM all_tab_cols
 WHERE table_name = :tablename`, {
    tablename: tablename
  }
);
