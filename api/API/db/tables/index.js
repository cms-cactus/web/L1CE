var restify = require('restify');
var moment = require('moment');
var tools = require.main.require('./API/tools.js');
var db = require.main.require('./database.js');
var root = tools.getAPIRoot(__dirname);
var passport = global.passport;
var getConstraints = require("./getConstraints.js");
var getTableInfo = require("./getTableInfo.js");
var getPrimaryColumn = require("./getPrimaryColumn.js");
var getTable = require("./getTable.js").getTable;
var getTableRow = require("./getTable.js").getTableRow;
var getClob = require("./getClob.js");
var insertNewRow = require("./insertNewRow.js");
var subsystems =  require("../subsystems/getSubsystemTables.js");
var getTableOwner = require("./getTableOwner.js");
var {validate, invalidate} = require("./invalidation.js");

module.exports = server => {

  server.get(`${root}/`,
    passport.jwt.requireReadMode,
    (request, response, next) => {
      subsystems.getSubsystemTables({
        credentials: request.user
      })
      .then(subsystemTables => {
        Object.keys(subsystemTables).filter( subsystem => subsystem != "_links").forEach( subsystem => Object.keys(subsystemTables[subsystem].blocks).forEach( key => response.addLink("get", `${root}/${key}`, `blocks:${key}`) ));
        response.send({});
      })
      .catch( error => response.send(500, error))
    }
  );

  server.get(`${root}/:tableName`,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      /**
       * HAL: get full table content
       * @name getTable
       */

      var tableName = request.params.tableName;
      getTable({
        tableName: tableName,
        credentials: request.user
      })
      .then( ({table, owner, primaryColumn}) => {

        // we don't send full tables, only links to keys in the table
        // add links to keys
        table.rows.forEach( row => {
          var key = encodeURIComponent(row[primaryColumn]);
          response.addLink("get", `${root}/${request.params.tableName}/${key}`, `keys:${key}`);
        })
        response.send({});
      })
      .catch( error => {
        response.send(503, error);
      });

    });

  server.get(`${root}/:tableName/schema`,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      var tableName = request.params.tableName;
      getTableOwner({
        tableName: tableName,
        credentials: request.user
      })
      .then( owner => db.getConnection({
        credentials: request.user,
        databaseUser: owner
      }))
      .then( connection => getTableInfo(connection, tableName) )
      .then( schema => {
        response.send(schema);
      })
      .catch(error => {
        console.error(error);
        response.send(500, error);
      });
    }
  );

  server.get(`${root}/:tableName/constraints`,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      var tableName = request.params.tableName;
      getTableOwner({
        tableName: tableName,
        credentials: request.user
      })
      .then( owner => db.getConnection({
        credentials: request.user,
        databaseUser: owner
      }))
      .then( connection => getConstraints(connection, tableName) )
      .then( constraints => {
        response.send(constraints);
      })
      .catch(error => {
        console.error(error);
        response.send(500, error);
      });
    }
  );

  server.post(`${root}/:tableName/getkeys`,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      var tableName = request.params.tableName;
      var validOnly = request.params.validonly != undefined ? request.params.validonly : false;
      getKeys(tableName, request.user, validOnly, "undefined", response);
     }
  );

  server.post(`${root}/:tableName/:currentKey/getkeys`,
    passport.jwt.requireReadMode,
    server.sanitizeKeyname,
    (request, response, next) => {
      var tableName = request.params.tableName;
      var validOnly = request.params.validonly != undefined ? request.params.validonly : false;
      var encodedCurrentKey = request.params.currentKey;
      getKeys(tableName, request.user, validOnly, encodedCurrentKey, response);
     }
  );

  server.post(`${root}/:tableName/getvalidkeys`,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      var tableName = request.params.tableName;
      getKeys(tableName, request.user, true, "undefined", response);
     }
  );

  server.post(`${root}/:tableName/:currentKey/getvalidkeys`,
    passport.jwt.requireReadMode,
    server.sanitizeKeyname,
    (request, response, next) => {
      var tableName = request.params.tableName;
      var encodedCurrentKey = request.params.currentKey;
      getKeys(tableName, request.user, true, encodedCurrentKey, response);
     }
  );

/// find better way than to duplicate code....
  var getKeys = (tableName, credentials, validOnly, encodedCurrentKey, response) => {
    getTable({
      tableName: tableName,
      credentials: credentials
    })
    .then( ({table, owner, primaryColumn}) => {
      var filtered = {
        rows: [],
        metaData: [],
        owner: owner,
        validation: table.validation,
        mode_column: table.mode_column
      }

      filtered.metaData = table.metaData.filter( column => column.name == primaryColumn || column.name == "VALID" || column.name == "NAMESPACE" || column.name == "CATEGORY" || column.name == table.mode_column);

      filtered.rows = table.rows.map( row => ({
        [primaryColumn]: row[primaryColumn],
        VALID: row.VALID,
        MODE_VALID: row.MODE_VALID,
        VERSION: row.VERSION,
        NAMESPACE: row.NAMESPACE,
        CATEGORY: row.CATEGORY,
        MODE: row[table.mode_column],
        YEAR: moment( row.CREATION_DATE, "DD-MMM-YY hh.mm.ss.SSSSSS a" ).year()
      }));


      var templateRow = false;
      if ( encodedCurrentKey && encodedCurrentKey != "undefined" ) {
        var currentKey = decodeURIComponent( encodedCurrentKey );
        templateRow = filtered.rows.find( row => row.ID == currentKey );
        if ( validOnly ) {
          filtered.rows = filtered.rows.filter( row => row.VALID == "Y" || row.ID == currentKey );
        }
      } else if ( validOnly ) {
        filtered.rows = filtered.rows.filter( row => row.VALID == "Y" );        
      }


      if ( templateRow && templateRow.NAMESPACE ) {
        filtered.rows = filtered.rows.filter( row => row.NAMESPACE == templateRow.NAMESPACE);
      }

      response.send(filtered);
    })
    .catch( error => {
      console.error(error);
      response.send(500, error);
    });
  }


  var getTableKey = (request, response, next) => {
    var {tableName, primaryKey} = request.params;
    getTableRow({
      tableName: tableName,
      primaryKey: primaryKey,
      credentials: request.user
    })
    .then( ({table, primaryColumn, owner}) =>
      db.getConnection({
        credentials: request.user,
        databaseUser: owner
      })
      .then( connection => {
        var clobColumns = table.metaData.filter( column => {
          return column.data_type == "CLOB"
        });
        var arrClobPromises = clobColumns.map( column => getClob({
          connection: connection,
          tableName: tableName,
          primaryColumn: primaryColumn,
          primaryKey: primaryKey,
          clobColumnName: column.name
        }));
        var arrClobInput = clobColumns.map( column => ({
          tableName: tableName,
          primaryColumnName: primaryColumn,
          primaryKey: primaryKey,
          clobColumnName: column.name
        }))
        return Promise.all(arrClobPromises)
        .then( clobs => {
          for (var i = 0; i < clobs.length; i++) {
            var clob = clobs[i];
            var info = arrClobInput[i];
            table.row[info.clobColumnName] = clob;
          }
          response.send(200, table);
        })
      })
    )
    .catch( error => {
      if (error.code) {
        response.send(error.code, error);
      } else {
        response.send(500, error)
      }
    });
  }

  server.get(`${root}/:tableName/:primaryKey`,
    passport.jwt.requireReadMode,
    server.sanitize,
    getTableKey
  );

  server.post(`${root}/:tableName`,
    passport.jwt.requireWriteMode,
    server.sanitize,
    (request, response, next) => {
      if (!request.body) {
        response.send(401, {message: "no table name supplied"})
      }
      var tableName = request.params.tableName;
      var {row: newRow, sourceKey} = request.body;

      insertNewRow({
        tableName: tableName,
        row: newRow,
        sourceKey: sourceKey,
        credentials: request.user
      })
      .then( ({newRow, primaryColumn}) => {
        request.params.primaryKey = newRow[primaryColumn];
        return getTableKey(request, response);
      })
      .catch( error => response.send(error.code || 500, {
        code: error.code || 500,
        message: error.message || error
      }));
    }
  );

  server.post(`${root}/:tableName/:primaryKey/invalidate`,
    passport.jwt.requireWriteMode,
    server.sanitize,
    (request, response, next) => {
      /**
       * HAL: invalidates a given key
       * This is only possible on tables that have manual validation
       * Tables with automatic validation use the key version to determine validity
       * @name invalidateKey
       */
      var {tableName, primaryKey: keyName} = request.params;
      invalidate({
        tableName: tableName,
        keyName: keyName,
        credentials: request.user
      })
      .then( message => response.send(200, {message}))
      .catch( error => response.send(error.code || 500, error.message || error));
    }
  );

  server.post(`${root}/:tableName/:primaryKey/validate`,
    passport.jwt.requireWriteMode,
    server.sanitize,
    (request, response, next) => {
      /**
       * HAL: validates a given key
       * This is only possible on tables that have manual validation
       * Tables with automatic validation use the key version to determine validity
       * @name invalidateKey
       */
      var {tableName, primaryKey: keyName} = request.params;
      validate({
        tableName: tableName,
        keyName: keyName,
        credentials: request.user
      })
      .then( message => response.send(200, {message}))
      .catch( error => response.send(error.code || 500, error.message || error));
    }
  );

  server.post(`${root}/:tableName/:primaryKey/getclob`,
    passport.jwt.allowTokenInParams,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      var tableName = request.params.tableName;
      var primaryKey = request.params.primaryKey;
      var clobColumnName = request.params.clobColumnName;

      getTableRow({
        tableName: tableName,
        primaryKey: primaryKey,
        credentials: request.user
      })
      .then( ({table, primaryColumn, owner}) => {
        return db.getConnection({
          credentials: request.user,
          databaseUser: owner
        })
        .then( connection => getClob({
          connection: connection,
          tableName: tableName,
          primaryColumn: primaryColumn,
          primaryKey: primaryKey,
          clobColumnName: clobColumnName
        }) );
      })
      .then( clob => {
        var contentType = "text/xml";
        // detect XML or JSON
        try {
          JSON.parse(clob);
          contentType="application/json";
        } catch (e) {
          // it's not json, assume it is XML
        } finally {
          var headers = {
            // 'Content-Length': Buffer.byteLength(clob),
            'Content-Type': contentType
          }

          if (typeof request.params.download == "string") {
            var filename = `${tableName}_${primaryKey}_${clobColumnName}`;
            filename += contentType == "text/xml" ? ".xml" : ".json";
            headers['Content-Disposition'] = `attachment; filename="${filename}"`;
          }
          response.writeHead(200, headers);
          response.write(clob);
          response.end();
        }
      })
      .catch(error => {
        response.send(503, error);
      });

    });

  server.post(`${root}/:tableName/search`,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      var tableName = request.params.tableName;
      var rowFilter = request.params.rowFilter;
      var columnFilter = request.params.columnFilter;
      if (! (rowFilter || columnFilter)) {
        response.send(new restify.errors.BadRequestError("rowFilter or columnFilter parameter is missing"));
      }

      getTable({
        tableName: tableName,
        credentials: request.user
      })
      .then( ({table, owner, primaryColumn}) => {

        // var queryFactory = require("rql/js-array").query;
        // var columnQuery = queryFactory(columnFilter);
        // var rowQuery = queryFactory(rowFilter);

        try {
          var metaData = columnQuery(table.metaData);
          var rows = rowQuery(table.rows);

          var newRows = [];
          for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            var newRow = {};
            for (var j = 0; j < metaData.length; j++) {
              var columnName = metaData[j].name;
              newRow[columnName] = row[columnName];
            }
            newRows.push(newRow);
          }


          var result = {
            rows: newRows,
            metaData: metaData
          };
          response.send(result);
        } catch (e) {
          console.error(e);
          response.send(new restify.errors.BadRequestError("submitted query is invalid"));
        }
      })
      .catch( error => {
        console.error(error);
        response.send(500, error);
      });

    });

}
