var getConstraints = require("./getConstraints.js");
var getTableInfo = require("./getTableInfo.js");
var getTableOwner = require("./getTableOwner.js");
var db = require.main.require('./database.js');

var _constraintMap = {
  "C": "check",
  "P": "primary",
  "U": "unique",
  "R": "reference",
  "V": "view",
  "O": "read-only view"
}
var getPrettyConstraintType = constraint_type => _constraintMap[constraint_type] || constraint_type;

module.exports.getTableRow = ({tableName, primaryKey, credentials}) => new Promise( (jep, nope) => {
  getTable({
    tableName: tableName,
    credentials: credentials
  })
  .then( ({table, owner, primaryColumn}) => {
    // find the row
    // RQL doesn't like URL components (like '/' in 'keyname/v1')
    var row = table.row = table.rows.find( row => row[primaryColumn] == primaryKey)

    if (!row) {
      nope({
        code: 404,
        message: `Primary key ${primaryKey} does not exist in ${tableName}`
      })
    } else {
      delete table.rows;
      jep({table: table, primaryColumn: primaryColumn, owner: owner});
    }
  })
  .catch( error => nope(`Error while getting table row: ${error}`));
});

var getTable = module.exports.getTable = ({tableName, credentials}) => new Promise( (ok, mitä_vittua) => {
  var tableOwner;
  getTableOwner({
    tableName: tableName,
    credentials: credentials
  })
  .then( owner => {
    tableOwner = owner;
    return db.getConnection({
      credentials: credentials,
      databaseUser: owner
    })
  })
  .then( connection => {
    var tableSelector = tableOwner.isSnowFlake ? tableOwner.isSnowFlake+"."+tableName : tableName;
    return Promise.all([
      connection.streamExecute(`SELECT * FROM ${tableSelector}`),
      getConstraints(connection, tableName, tableOwner.isSnowFlake ? tableOwner.isSnowFlake : tableOwner),
      getTableInfo(connection, tableName),
      connection
    ])
  })
  .then( ([table, constraints, schema, connection]) => {

    /*********************
     * enrich metadata   *
     *********************/
    var primaryColumn;
    var hasValidColumn = table.metaData.some( column => column.name == "VALID");
    var hasVersionColumn = table.metaData.some( column => column.name == "VERSION");
    var mode_column = table.metaData.find( column => column.name == "KEYNAME" || column.name == "L1_HLT_MODE"  || column.name == "L1_TRG_MODE" || column.name == "CMS_RUN_MODE");
    var nameSpaceColumn = table.metaData.find( column => column.name == "NAMESPACE");
    if (!mode_column) {
      mode_column = nameSpaceColumn;
    }
    if (mode_column) {
      table.mode_column = mode_column.name;
    }
    table.owner = tableOwner;
    table.metaData.forEach( column => {
      column.constraints = column.constraints || [];

      // put contraints (e.g. primary key) in table metaData
      constraints.rows.forEach( constraint => {
        if (column.name == constraint.COLUMN_NAME) {
          column.constraints.push(getPrettyConstraintType(constraint.CONSTRAINT_TYPE));
          if (constraint.CONSTRAINT_TYPE == "R") {
            column.FOREIGN_TABLE_NAME = constraint.FOREIGN_TABLE_NAME.substring( constraint.FOREIGN_TABLE_NAME.indexOf('.')+1 );
            column.FOREIGN_COLUMN_NAME = constraint.FOREIGN_COLUMN_NAME;
            column.FOREIGN_KEY_STATUS = constraint.FOREIGN_KEY_STATUS;
          }
          if (constraint.CONSTRAINT_TYPE == "P") {
            if (primaryColumn) {
              throw new Error(`table or view ${tableName} contains at least 2 primary keys. Found '${primaryColumn.name}' and '${column.name}'`);
            }
            primaryColumn = column;
          }
        }
      });

      // put column data types in table metadata
      schema.rows.forEach( column_schema => {
        if (column.name == column_schema.COLUMN_NAME) {
          column.data_type = column_schema.DATA_TYPE;
          column.data_length = column_schema.DATA_LENGTH;
          column.nullable = column_schema.NULLABLE == "Y";
        }
      });

      // describe if the user needs to send this column when inserting a new row
      column.ignored_at_insert = column.name == "ID" || column.name == "CREATION_DATE" || column.name == "AUTHOR" || column.name == "CREATION_AUTHOR" || column.name == "VALID" || column.name == "MODE_VALID" || (!hasValidColumn && column.name == "VERSION");

    });

    if (!primaryColumn) {
      var firstUniqueColumn = table.metaData.find( column => (column.constraints || []).some(constraint => constraint.CONSTRAINT_TYPE == "U") )
      if (firstUniqueColumn) {
        console.warn(`table or view ${tableName} has no primary key! Resorting to using the first encountered unique key (${firstUniqueColumn.name})`);
        primaryColumn = firstUniqueColumn;
      }
      else {
        console.warn(`table or view ${tableName} has no primary or unique key! Resorting to using the first encountered column (${table.metaData[0].name})`);
        primaryColumn = table.metaData[0];
      }
      console.warn(`please find a better database designer`);
    }

    if (hasValidColumn) {
      table.validation = "manual";
      //console.debug(`table ${tableName} has a VALID column -> manual validation`);
    }
    else {
      if (!hasVersionColumn) {
        console.warn(`table ${tableName} has no VALID nor a VERSION column, versioning not possible! Considering all rows valid.`);
        table.validation = "unset"
      } else {
        table.validation = "automatic";
      }
      //console.debug(`table ${tableName} has no VALID column -> automatic validation`);
      table.metaData.push({
        name: "VALID",
        ignored_at_insert: true,
        byteSize: 1,
        constraints: [],
        data_length: 1,
        data_type: "CHAR",
        dbType: 96,
        fetchType: 2001,
        nullable: false
      });
    }

    if (mode_column) {
      //console.warn(`table ${tableName} has MODE column, adding MODE_VALID column`);
      table.metaData.push({
        name: "MODE_VALID",
        ignored_at_insert: true,
        byteSize: 1,
        constraints: [],
        data_length: 1,
        data_type: "CHAR",
        dbType: 96,
        fetchType: 2001,
        nullable: false
      });
    }

    /************************
     * add validation info *
     ************************/
    if (hasValidColumn && hasVersionColumn) {
      // exception case, both 'VERSION' (automatic validation) and 'VALID' (manual validation)
      // are present, output a warning
      console.warn(`table ${tableName} has both a 'VERSION' and a 'VALID' column -> using manual validation`);

    } else if (!hasValidColumn && !hasVersionColumn) {
      // exception case, neither 'VERSION' (automatic validation) and 'VALID' (manual validation)
      // are present, fallback to manual validation and validate everything
      table.rows.forEach( row => row.VALID = "Y" );

    }

    if (!mode_column && !hasValidColumn && hasVersionColumn) {
      /// only version.... should not exists
      var latestVersion = 0;
      table.rows.forEach( row => {
        var version = Number(row.VERSION);
        if (version > latestVersion) {
          latestVersion = version;
        }
      });
      table.rows.forEach( row => {
        row.VALID = Number(row.VERSION) == latestVersion ? "Y" : "N";
      });
    }
    if (mode_column && hasVersionColumn) {
      // automatic validation, only the latest version of a mode is valid

      // get the list of modes
      var modeTableName = mode_column.FOREIGN_TABLE_NAME;
      var modeTablePColumn = mode_column.FOREIGN_COLUMN_NAME;
      connection.streamExecute(`select ${modeTablePColumn} from ${modeTableName} where VALID='Y'`)
      .then( modeTable =>
        modeTable.rows.reduce( (r, row) => {
          r[row[modeTablePColumn]] = true;
          return r;
        }, {} )
      )
      .then( validModes => {
        // get the latest version of each mode
        var modes = {};
        table.rows.forEach( row => {
          var mode = nameSpaceColumn ? `${row["NAMESPACE"]}_${row[mode_column.name]}` : row[mode_column.name];
          var version = Number(row.VERSION);
          if (!modes[mode] || modes[mode] < version) { // if this row has a higher version
            modes[mode] = version;
          }
        });

        // go over all the rows again, determine if validated
        table.rows.forEach( row => {
          // if the mode is invalid, the key is invalid
          var modeName = row[mode_column.name];
          if (validModes[modeName]) {
            var fullModeName = nameSpaceColumn ? `${row["NAMESPACE"]}_${modeName}` : modeName;
            /// this takes into account the L1_MENU special case
            if ( !hasValidColumn ) {
              row.VALID = Number(row.VERSION) == modes[fullModeName] ? "Y" : "N";
            }
            row.MODE_VALID = "Y";
          }
          else {
            row.VALID = "N";
            row.MODE_VALID = "N";
          }
        });

        ok({
          table: table,
          owner: tableOwner,
          primaryColumn: primaryColumn.name
        });
      })

    } else {
      ok({
        table: table,
        owner: tableOwner,
        primaryColumn: primaryColumn.name
      });
    }

  })
  .catch( error => {
    mitä_vittua(`Error while fetching table: ${error}`);
  });
});
