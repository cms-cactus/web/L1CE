var oracledb = require('oracledb');
module.exports = (connection, fulltablename, primaryColummnName, primaryKey, clobColumnName, clob) => new Promise( (ok, mitä_vittua) => {
  if (!(fulltablename && primaryColummnName && primaryKey && clobColumnName && clob)) {
    // console.log(fulltablename, primaryColummnName, primaryKey, clobColumnName, clob);
    mitä_vittua("not enough parameters");
  } else {
    connection.execute(
      // thank you shitty data binding
      `UPDATE ${fulltablename} SET ${clobColumnName} = EMPTY_CLOB() WHERE ${primaryColummnName} = :primaryKey RETURNING ${clobColumnName} INTO :lobbv`,
      {
        primaryKey: primaryKey,
        lobbv: {
          type: oracledb.CLOB,
          dir: oracledb.BIND_OUT
        }
      }
    )
    .then( result => {
      var lob = result.outBinds.lobbv[0];
      if (!lob) {
        mitä_vittua("CLOB not found");
      }
      // makes the lob a string instead of a buffer
      lob.setEncoding('utf8');
      lob.on('finish', () => {
        ok(lob);
      });
      lob.on('error', error => mitä_vittua(`Error while fetching clob: ${error}`));
      lob.end(clob, 'utf8', () => {});
    })
    .catch( error => {
      mitä_vittua(`Error while setting clob: ${error}`);
    });
  }
});
