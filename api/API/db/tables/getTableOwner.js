var db = require.main.require('./database.js');
var config = require.main.require("./config");

var ownerCache = {};

module.exports = ({connection, tableName, credentials}) => new Promise( (ok, nope) => {
  var database = credentials.l1ce.database;
  var cacheHit = ownerCache[tableName+database]
  if (cacheHit) {
    ok(cacheHit);
  } else {
    (connection ? Promise.resolve(connection) : db.getConnection({credentials: credentials}))
    .then( connection => Promise.all([
      connection.streamExecute(
        `select owner from all_tables where table_name = :table_name`, {
          table_name: tableName
        }
      ),
      connection.streamExecute(
        `select owner from all_views where view_name = :view_name`, {
          view_name: tableName
        }
      )
    ]))
    .then( ([tables, views]) => {
      var dbusers = db.getUsersForDatabase(database);

      var all_owners = tables.rows.concat(views.rows);
      var owners = all_owners.map( row => row.OWNER.toLowerCase());
      // take the intersection of dbusers and owners
      var matches = dbusers.filter(n => owners.indexOf(n.toLowerCase()) != -1);

      // if there are no matches, this might be a snowflake
      // we piggyback an unconfigured db user on another one and flag it
      if (matches.length == 0) {
        var snowFlakeOwner = owners.find( snowFlake => config.databases.users.overrides[snowFlake] );
        if (snowFlakeOwner) {
          matches.push(new String(config.databases.users.overrides[snowFlakeOwner]));
          matches[0].isSnowFlake = snowFlakeOwner;
          console.warn(`special snowflake ${snowFlakeOwner}; Using ${matches[0]}`);
        }
      }

      if (owners.length == 0) {
        console.error(`database returned 0 owners for table or view ${tableName}`);
        console.error(`query: "select table_name, owner from all_tables where table_name = ${tableName}"\nresult:`, tables, views);
        throw `database returned 0 owners for table or view ${tableName}`;
      }

      else if (matches.length == 0) {
        console.error(`none of the owners for table ${tableName} are configured in L1CE.\nowners for table or view ${tableName}:`, owners, "\nconfigured db users:", dbusers);
        console.error(`query: "select table_name, owner from all_tables where table_name = ${tableName}"\nresult:`, tables, views);
        throw `none of the owners for table or view ${tableName} are configured in L1CE`;
      }

      else if (matches.length > 1) {
        console.error(`table or view ${tableName} has multiple owners, and >1 of these owners is configured in L1CE`);
        throw `table or view ${tableName} has multiple owners, and >1 of these owners is configured in L1CE`;
      }

      else {
        var owner = ownerCache[tableName+database] = matches[0];
        //console.debug(`owner for table ${tableName} is ${owner}`);
        ok(owner);
      }
    })
    .catch( error => nope(`Error while getting table owner: ${error}`));
  }
});
