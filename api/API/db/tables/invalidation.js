var db = require.main.require('./database.js');
var getTableRow = require("./getTable.js").getTableRow;
var { io_promise } = require.main.require("./makeWebSocket.js");


module.exports.invalidate = ({tableName, keyName, credentials}) =>
  getTableRow({
    tableName: tableName,
    primaryKey: keyName,
    credentials: credentials
  })
  .then( ({table, owner, primaryColumn}) => {
    if (table.validation == "automatic") {
      throw {
        code: 409,
        message: `cannot invalidate keys in a table with automatic validation`
      }
    }
    else if (table.row.VALID == "N") {
      throw {
        code: 409,
        message: `key ${keyName} in ${tableName} is already invalid`
      };
    } else {
      return {owner: owner, primaryColumn: primaryColumn};
    }
  })
  .then( ({owner, primaryColumn}) => {
    return db.getConnection({
      credentials: credentials,
      databaseUser: owner
    })
    .then( connection => connection.execute(`
      UPDATE ${tableName}
      SET
      VALID='N'
      WHERE
      ${primaryColumn}=:INVALID_KEY`,
      {
        INVALID_KEY: keyName
      })
    )
    .then( result => owner)
  })
  .then( owner => {
    return io_promise.then(io => io.notifyInvalidation({
      tableName: tableName,
      tableOwner: owner,
      primaryKey: keyName,
      credentials: credentials
    }))
    .then(() => `key ${keyName} in ${tableName} is invalidated`)
  });

module.exports.validate = ({tableName, keyName, credentials}) =>
  getTableRow({
    tableName: tableName,
    primaryKey: keyName,
    credentials: credentials
  })
  .then( ({table: {row: row}, owner, primaryColumn}) => {
    if (row.VALID == "Y") {
      throw {
        code: 409,
        message: `key ${keyName} in ${tableName} is already valid`
      };
    } else {
      return {owner: owner, primaryColumn: primaryColumn};
    }
  })
  .then( ({owner, primaryColumn}) => {
    return db.getConnection({
      credentials: credentials,
      databaseUser: owner
    })
    .then( connection => connection.execute(`
      UPDATE ${tableName}
      SET
      VALID='Y'
      WHERE
      ${primaryColumn}=:keyName`,
      {
        keyName: keyName
      })
    )
    .then( result => owner)
  })
  .then( owner => io_promise.then(io => io.notifyValidation({
    tableName: tableName,
    tableOwner: owner,
    primaryKey: keyName,
    credentials: credentials
  })))
  .then(() => `key ${keyName} in ${tableName} is validated`);
