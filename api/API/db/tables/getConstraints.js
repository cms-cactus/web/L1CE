module.exports = (connection, tablename, owner) => new Promise( (ok, damn) => {
  if (!owner) {
    owner = connection.userName;
  }
  Promise.all([
    connection.streamExecute(
      `SELECT
         columns.table_name,
         columns.column_name,
         constraints.constraint_type,
         constraints.constraint_name,
         constraints.owner
       FROM
         all_constraints constraints,
         all_cons_columns columns
       WHERE columns.table_name = :tablename
         AND constraints.constraint_name = columns.constraint_name
         AND constraints.owner = columns.owner
         AND constraints.owner = :owner`,
      {
          tablename: tablename,
          owner: owner.toUpperCase()
      }
    ),
    connection.streamExecute(
      /***************** GET TABLE + COLUMNS FOR FK CONSTRAINTS *****************
      * all_constraints always contains two related rows,
      * match r_constraint_name with constraint_name
      * -> give table_name, you get the remote table name and a constraint_name
      * all_cons_columns contains columns linked with constraint_name
      * -> give constraint_name, you get the column name
      **************************************************************************/
      `SELECT
        ac.constraint_name,
        ac.table_name,
        ac.r_owner as owner,
        ac.status as foreign_key_status,
        ac2.table_name as foreign_table_name,
        acc.column_name as foreign_column_name
       FROM
        all_constraints ac
       INNER JOIN
        all_constraints ac2
       ON
        ac2.constraint_name = ac.r_constraint_name
       INNER JOIN
        all_cons_columns acc
       ON
        acc.constraint_name = ac.r_constraint_name
       WHERE
        ac.table_name = :tablename and
        ac.constraint_type = 'R'`,
      {
        tablename: tablename
      }
    )
  ])
  .then( ([constraints, foreign_key_info]) => {

    constraints.metaData.push(foreign_key_info.metaData[1]);
    constraints.metaData.push(foreign_key_info.metaData[2]);

    constraints.rows.forEach( constraint => {
      foreign_key_info.rows.forEach( fk => {
        if (constraint.CONSTRAINT_NAME == fk.CONSTRAINT_NAME) {
          constraint.FOREIGN_TABLE_NAME = `${fk.OWNER}.${fk.FOREIGN_TABLE_NAME}`;
          constraint.FOREIGN_COLUMN_NAME = fk.FOREIGN_COLUMN_NAME;
          constraint.FOREIGN_KEY_STATUS = fk.FOREIGN_KEY_STATUS;
        }
      });
      if (constraint.CONSTRAINT_TYPE == "R" && !constraint.FOREIGN_TABLE_NAME) {
        console.error("no foreign table found for column", constraint.COLUMN_NAME);
      }
    });

    ok(constraints)
  }).
  catch( error => {
    damn(`Error while getting constraints: ${error}`);
  })
});
