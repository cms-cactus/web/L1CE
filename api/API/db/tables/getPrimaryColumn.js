module.exports = constraints => new Promise( (ok, damn) => {
  var primaryKey;
  for (var i = 0; i < constraints.rows.length; i++) {
    var constraint = constraints.rows[i];
    if (constraint.CONSTRAINT_TYPE == "P") {
      if (primaryKey) {
        damn(`table ${params.tablename} has more than one primary key (${primaryKey} and ${constraint.COLUMN_NAME} found)`);
      }
      primaryKey = constraint.COLUMN_NAME;
    }
  }
  if (!primaryKey) {
    damn(`table ${params.tablename} contains no primary key`);
  } else {
    ok(primaryKey);
  }
});
