module.exports = (connection, fulltablename, primaryColummnName, primaryKey, clobColumnName) => new Promise( (ok, mitä_vittua) => {
  if (!(fulltablename && primaryColummnName && primaryKey && clobColumnName)) {
    mitä_vittua("not enough parameters");
  } else {
    connection.execute(
      // thank you shitty data binding
      `select ${clobColumnName} from ${fulltablename} where ${primaryColummnName} = :primaryKey`,
      {
        primaryKey: primaryKey
      }
    )
    .then( result => {
      var lob = result.rows[0][clobColumnName];
      if (!lob) {
        ok("");
      }
      var clob = "";
      // makes the lob a string instead of a buffer
      lob.setEncoding('utf8');
      lob.on('data', chunk => clob += chunk);
      lob.on('end', () => ok(clob));
      lob.on('error', error => mitä_vittua(`Error while fetching clob reference: ${error}`));
    })
    .catch( error => {
      mitä_vittua(`Error while getting clob reference: ${error}`);
    });
  }
});
