
var tools = require.main.require('./API/tools.js');
var db = require.main.require('./database.js');
var root = tools.getAPIRoot(__dirname);

module.exports = server => {

  server.get(`${root}/`,
    passport.jwt.requireReadMode,
    (request, response, next) => {
      var databaseName = request.user.l1ce.database;
      var userName = request.user.cern.fullname;
      console.debug("DB changes:", db.databaseChanges);
      response.send(JSON.parse(JSON.stringify(db.databaseChanges[databaseName])));
    }
  );

  server.post(`${root}/commit`,
    passport.jwt.requireWriteMode,
    (request, response, next) => {
      if (request.user.cern.fullname == "testing bot") {
        response.send(403, {code: 403, message: "cannot commit changes for a testing token"});
      }
      else {
        db.commit(request.user)
        .then( () => response.send(200, {}) )
        .catch( error => response.send(500, {code: 500, message: error.message}) );
      }
    }
  );
  server.post(`${root}/rollback`,
    passport.jwt.requireWriteMode,
    (request, response, next) => {
      db.rollback(request.user)
      .then( () => response.send(200, {}) )
      .catch( error => response.send(500, {code: 500, message: error.message}) );
    }
  );

}
