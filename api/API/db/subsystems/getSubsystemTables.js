var db = require.main.require('./database.js');
var tools = require.main.require('./API/tools.js');
var subsystemTables = {};
var config = require.main.require("./config");

var cacheInvalidTimeout = config.authentication.token_timeout * 1000 * 60; // minutes

var invalidate = () => {
  if ( subsystemTables != {} ) {
    console.log( "Subsystem cache invalidated");
  }
  subsystemTables = {};
  timeout = setTimeout(invalidate, cacheInvalidTimeout);
};


var timeout = setTimeout(invalidate, cacheInvalidTimeout);


/*
 * make timeouts accessible from outside
*/

// module.exports.cacheTimeout = () => { 
//   return timeout
// };


module.exports.resetTimeout = () => {
  clearTimeout( timeout );
  console.log( "Extending Subsystem cache validity of", config.authentication.token_timeout, "minutes" );
  timeout = setTimeout( invalidate, cacheInvalidTimeout );
};

module.exports.invalidateCache = () => {
  clearTimeout( timeout );
  invalidate();
};

/*
 * getSubsystemTables
*/

module.exports.getSubsystemTables = ({credentials}) => new Promise((ok, damn) => {

  var database = credentials.l1ce.database;
  if (subsystemTables[database]) {
    ok(subsystemTables[database]);
  } else {
    db.getConnection({
      credentials: credentials
    })
    .then( connection => connection.streamExecute("select user_name, table_name, subsystem, block, block_alias, is_top from CMS_TRG_L1_CONF.L1CE_CONFIGS") )
    .then( rawResult => {
      subsystemTables[database] = rawResult.rows.reduce( (r, {USER_NAME, TABLE_NAME, SUBSYSTEM, BLOCK, BLOCK_ALIAS, IS_TOP}) => {
        if (!r[SUBSYSTEM]) {
          r[SUBSYSTEM] = {
            username: USER_NAME,
            top: IS_TOP == "Y",
            blocks: {}
          }
        }
        if (r[SUBSYSTEM].blocks[BLOCK]) {
          console.warn(`Block ${BLOCK} for subsystem ${SUBSYSTEM} is already set to ${r[subsystem].blocks[BLOCK]}, ignoring ${TABLE_NAME}`);
        }
        else {
          r[SUBSYSTEM].blocks[BLOCK] = {
            table: TABLE_NAME,
            alias: BLOCK_ALIAS
          };
        }
        return r;
      }, {});
      ok(subsystemTables[database]);
    })
    .catch(error => {
      damn(`Error while getting subsystem tables: ${error}`);
    });
  }
});
