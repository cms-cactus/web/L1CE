var restify = require('restify');
var tools = require.main.require('./API/tools.js');
var db = require.main.require('./database.js');
var root = tools.getAPIRoot(__dirname);
var subsystems = require("./getSubsystemTables.js")
var passport = global.passport;

module.exports = server => {
  server.get(`${root}/`,
    passport.jwt.requireReadMode,
    (request, response, next) => {
      // gets list of users
      subsystems.getSubsystemTables({credentials: request.user})
      .then(subsystemTables => {
        response.send(subsystemTables);
      })
      .catch(error => {
        response.send(500, error);
      });
    }
  );

  server.get(`${root}/:subsystemName`,
    passport.jwt.requireReadMode,
    server.sanitize,
    (request, response, next) => {
      subsystems.getSubsystemTables({credentials: request.user})
      .then(subsystemTables => {
        if (!subsystemTables[request.params.subsystemName]) {
          return next(new restify.errors.NotFoundError(`no subsystem with name ${request.params.subsystemName} was found`));
        }
        response.send(subsystemTables[request.params.subsystemName]);
      })
      .catch(error => {
        response.send(500, error);
      });
    }
  );
}
