var tools = require.main.require('./API/tools.js');
var db = require.main.require('./database.js');
var root = tools.getAPIRoot(__dirname);

module.exports = server => {

  require("./subsystems/index.js")(server);
  require("./tables/index.js")(server);
  require("./changes/index.js")(server);
  var validateMenu = require("./validateMenu.js");

  server.get(`${root}/capabilities`, (request, response, next) => {
    response.send(db.capabilities);
  });

  server.get(`${root}/`, (request, response, next) => {
    response.send({});
  });

  server.get(`${root}/validateKey/:l1HltKey`,
    //server.sanitize,
    passport.jwt.requireReadMode,
    (request, response, next) => {

    var l1HltCONFKey = decodeURIComponent( request.params.l1HltKey );
    db.getConnection({
      credentials: request.user,
      databaseUser: 'cms_trg_r'
    })
    .then( connection => validateMenu.validateKey( l1HltCONFKey, connection ) )
    .then( tuple => { response.send(tuple); } )
    .catch( error => {
        console.error(error);
        response.send(500, error);
    });
  });

  server.post(`${root}/validateKeys`,
    //server.sanitize,
    passport.jwt.requireReadMode,
    (request, response, next) => {

    var hltCONFKey = request.params.hltKey;
    var l1CONFKey = request.params.trgKey;
    var l1RSKey = request.params.rsKey;
    db.getConnection({
      credentials: request.user,
      databaseUser: 'cms_trg_r'
    })
    .then( connection => validateMenu.validateKeys( hltCONFKey, l1CONFKey, l1RSKey, connection ) )
    .then( tuple => { response.send(tuple); } )
    .catch( error => {
        console.error(error);
        response.send(500, error);
    });
  });

}
