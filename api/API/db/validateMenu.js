"use strict";
var clob = require.main.require("./API/db/tables/getClob");

module.exports.validateKey = (l1HltKey, connection) =>
  new Promise((ok, nope) => {
    Promise.all([
      connection.streamExecute(
        `select HLT_KEY, ALGO_PRESCALE, L1_MENU_KEY from CMS_L1_HLT.V_L1_UGT_CONF where ID='${l1HltKey}'`
      )
    ]).then(([{ rows: l1Tuple }]) => {
      if (!l1Tuple.length) {
        nope(
          `no resuls from query: select HLT_KEY, ALGO_PRESCALE, L1_MENU_KEY from CMS_L1_HLT.V_L1_UGT_CONF where ID='${l1HltKey}'`
        );
      }
      var hltKey = l1Tuple[0].HLT_KEY;
      var algoPrescale = l1Tuple[0].ALGO_PRESCALE;
      var l1MenuKey = l1Tuple[0].L1_MENU_KEY;
      ok(this.validate(hltKey, algoPrescale, l1MenuKey, connection));
    });
  });

module.exports.validateKeys = (hltKey, trgConfKey, trgRsKey, connection) =>
  new Promise((ok, nope) => {
    Promise.all([
      connection.streamExecute(`select ugtconf.L1_MENU from CMS_TRG_L1_CONF.L1_TRG_CONF_KEYS conf
                                   left join CMS_TRG_L1_CONF.UGT_KEYS ugtconf
                                            on ugtconf.ID=conf.UGT_KEY where conf.ID='${trgConfKey}'`),
      connection.streamExecute(`select ugtrs.ALGO_PRESCALE from CMS_TRG_L1_CONF.L1_TRG_RS_KEYS rs
                                      left join CMS_TRG_L1_CONF.UGT_RS_KEYS ugtrs
                                      on ugtrs.ID=rs.UGT_RS_KEY where rs.ID='${trgRsKey}'`)
    ]).then(([{ rows: confTuple }, { rows: rsTuple }]) => {
      if (!confTuple.length) {
        nope(`no resuls from query: select ugtrs.ALGO_PRESCALE from CMS_TRG_L1_CONF.L1_TRG_RS_KEYS rs
                                                left join CMS_TRG_L1_CONF.UGT_RS_KEYS ugtrs
                                                on ugtrs.ID=rs.UGT_RS_KEY where rs.ID='${trgRsKey}'`);
      }
      if (!rsTuple.length) {
        nope(`no resuls from query: select ugtconf.L1_MENU from CMS_TRG_L1_CONF.L1_TRG_CONF_KEYS conf
                                     left join CMS_TRG_L1_CONF.UGT_KEYS ugtconf
                                                      on ugtconf.ID=conf.UGT_KEY where conf.ID='${trgConfKey}'`);
      }

      var algoPrescale = rsTuple[0].ALGO_PRESCALE;
      var l1MenuKey = confTuple[0].L1_MENU;
      ok(this.validate(hltKey, algoPrescale, l1MenuKey, connection));
    });
  });



module.exports.validate = (hltKey, algoPrescale, l1MenuKey, connection) => new Promise((ok, nope) => {
    console.log("ENTERING validate", hltKey, algoPrescale, l1MenuKey);

    Promise.all([
      clob({
        connection: connection,
        tableName: "CMS_TRG_L1_CONF.UGT_L1_MENU",
        primaryColumn: "ID",
        primaryKey: l1MenuKey,
        clobColumnName: "CONF"
      }),
      clob({
        connection: connection,
        tableName: "CMS_TRG_L1_CONF.UGT_RS_CLOBS",
        primaryColumn: "ID",
        primaryKey: algoPrescale,
        clobColumnName: "CONF"
      }),
      connection.streamExecute(
        `select * from table( CMS_HLT_GDR.findl1seeds('${hltKey}') )`
      ),
      connection.streamExecute(`with pq as
                           (SELECT J.ID, J.NAME, trim('{' from trim('}' from J.VALUE))
                             as PRESCALE_INDEX
                            FROM CMS_HLT_GDR.U_CONFVERSIONS A, CMS_HLT_GDR.U_CONF2SRV S, CMS_HLT_GDR.U_SRVELEMENTS J
                            WHERE A.NAME='${hltKey}' AND A.ID=S.ID_CONFVER AND J.NAME='lvl1Labels' AND J.ID_SERVICE=S.ID_SERVICE)
                        select myindex, regexp_substr (prescale_index, '[^,]+', 1, rn) pslabel
                        from pq
                        cross join (select rownum rn, mod(rownum -1, level) MYINDEX
                            from (select max (length (regexp_replace (prescale_index, '[^,]+'))) + 1 mx from pq )
                        connect by level <= mx ) where regexp_substr (prescale_index, '[^,]+', 1, rn) is not null order by myindex`)
      // `select cms_hlt_gdr.U_SRVELEMENTS.name, cms_hlt_gdr.U_SRVELEMENTS.value
      //    from cms_hlt_gdr.U_SRVELEMENTS, cms_hlt_gdr.U_confversions, cms_hlt_gdr.U_conf2srv
      //    where cms_hlt_gdr.U_conf2srv.id_confver=cms_hlt_gdr.u_confversions.id
      //      and cms_hlt_gdr.U_SRVELEMENTS.id_service=cms_hlt_gdr.U_conf2srv.id_service
      //      and cms_hlt_gdr.u_confversions.name='${hltKey}'
      //      and instr(cms_hlt_gdr.U_SRVELEMENTS.name,'lvl1')>0`
    ])
      .then(
        ([
          l1Menu,
          algoPrescale,
          { rows: hltTuple },
          { rows: hltPrescaleTuple }
        ]) => {
          var parser = require("xml2js").parseString;
          var util = require("util");
          var l1MenuSeeds = [];
          var hltMenuSeeds = [];
          var prescales = [];
          parser(l1Menu, (err, result) => {
            l1MenuSeeds = result["tmxsd:menu"]["algorithm"].map(x => x.name[0]);
          });
          parser(algoPrescale, (err, result) => {
            prescales = result["run-settings"][
              "context"
            ][0].param[1].columns[0].split(",");
          });

          for (var entry of hltTuple) {
            let tokens = entry.SEEDLOGICALEXP.split(" ");
            for (let token of tokens) {
              let seed = token.replace(/[()"'\s]/g, "");
              if (
                seed != "" &&
                seed != "OR" &&
                seed != "AND" &&
                seed != "NOT" &&
                hltMenuSeeds.indexOf(seed) == -1
              ) {
                hltMenuSeeds.push(seed);
              }
            }
          }

          var l1Prescales = prescales
            .map(x => x.trim().replace(":", ": "))
            .slice(1);
          var hltPrescales = [];
          for (var entry of hltPrescaleTuple) {
            hltPrescales.push(
              entry.MYINDEX + ": " + entry.PSLABEL.replace(/["']/g, "").trim()
            );
          }
          // console.log( "L1  PRESCALES", l1Prescales );
          // console.log( "HLT PRESCALES", hltPrescales );

          //var onlyL1 = l1MenuSeeds.filter( x => !hltMenuSeeds.includes(x) );
          var onlyHlt = hltMenuSeeds.filter(x => !l1MenuSeeds.includes(x));
          //console.log( "ONLY L1", onlyL1.length, "ONLY HLT", onlyHlt.length );
          console.log(
            "VALIDATION REPORT",
            onlyHlt.length,
            l1Prescales.length,
            hltPrescales.length,
            onlyHlt.length || l1Prescales.length != hltPrescales.length
              ? "failed"
              : "success"
          );
          ok({
            status:
              onlyHlt.length || l1Prescales.length != hltPrescales.length
                ? "failed"
                : "success",
            onlyHlt: onlyHlt,
            l1Seeds: l1MenuSeeds.length,
            hltSeeds: hltMenuSeeds.length,
            l1Prescales: l1Prescales,
            hltPrescales: hltPrescales
          });
        }
      )
      .catch(error => {
        console.log(error);
        nope({ status: "failed", reason: error });
      });
  });
