var tools = require.main.require('./API/tools.js');
var root = tools.getAPIRoot(__dirname);
var db = require.main.require('./database.js');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var htmlfile = fs.readFileSync(__dirname + '/saveJWT.html').toString();
var passport = global.passport;
var { io_promise } = require.main.require("./makeWebSocket.js");
var cryptokeys = require.main.require("./crypto/cryptokeys.js");
var { logHistory } = require.main.require('./logger.js');

var http = require('request-promise-native');
var config = require.main.require('./config.js');

/**
 * In order to manage our datbase connections, we track the validity
 * of our user's keys
 * If a key expires, all connections for that user must be closed
 * The API consumer (interface) is responsible for periodically renewing the token
 */

tokenTimeouts = {};
var sessionTimeout = config.authentication.token_timeout * 60; // minutes

const startTime = new Date();

module.exports = server => {
  require("./db/index.js")(server);

  server.get(root + '/', (request, response, next) => {
    response.send({});
  });

  makeJWTToken = (userInfo, duration = sessionTimeout ) => { // default is set in the configuration file config/dbconfig.js

    console.log( "The current mode is:", userInfo.l1ce.mode )
    if ( userInfo.l1ce.mode == "write" ) {
      require("./db/subsystems/getSubsystemTables.js").resetTimeout();
    }
    // else {
    //   db.rollback(userInfo, false); // rollback, but don't notify (read mode anyway)
    // }

    delete userInfo.exp;
    delete userInfo.iat;
    console.log(`Roles for ${userInfo.cern.fullname}:`, userInfo.l1ce.roles);
    return cryptokeys.then(cryptokeys => new Promise((ok, damn) => {
      var token = jwt.sign(userInfo, cryptokeys.privateKey, {
        expiresIn: duration,
        algorithm: 'RS256'
      });
      tokenTimeouts[userInfo.cern.fullname] !== undefined && clearTimeout(tokenTimeouts[userInfo.cern.fullname]);
      tokenTimeouts[userInfo.cern.fullname] = setTimeout( () => {
        console.log("token expired for", userInfo.cern.fullname);
        db.rollback(userInfo);
      }, duration * 1000 );

      ok(token);
    }));
  }

  server.get(`${root}/activeusers`, (request, response) => {
    io_promise.then(io => response.send(io.connectedUsers))
      .catch(err => response.code(500).send(err))
  })

  server.get(root + '/whoami',
    passport.jwt.authenticate,
    (request, response) => {
      response.send(request.user);
    }
  );

  server.get(root + '/logs', (request, response) => {
    const b = logHistory.join('');
    response.writeHead(200, {
      'Content-Type': 'text/plain',
      'Content-Length': Buffer.byteLength(b),
    });
    response.write(b);
    response.end();
  });

  server.get(root + '/starttime', (request, response) => response.send(startTime.toISOString()));

  /**
   * Exchanges a JWT Token for another one with different privileges for read/write mode and development/production database
   * @param token_data The decoded input token data
   * @param mode Requested mode ('read' or 'write')
   * @param database Requested database ('development' or 'production')
   */
  var exchangeToken = (token_data, newstuff) => new Promise((ok, nope) => {
    token_data.l1ce = token_data.l1ce || {};

    for (var key in newstuff) {
      if (newstuff.hasOwnProperty(key)) {
        var value = newstuff[key];
        if (key == "mode" && value != "read" && value != "write") {
          nope(`You requested a token for ${value} mode. Unfortunately this mode is not known by the system.`);
        } else if (key == "database" && !db.capabilities[value]) {
          nope(`You requested a token for the ${value} database. Unfortunately L1CE is not connected to this database. This could be because of missing configuration, or the database is not accessible.`);
        } else {
          token_data.l1ce[key] = value;
        }
      }
    }
    // if the user is in read mode, do a db rollback for that user
    // to prevent connection rotting
    // https://gitlab.cern.ch/cms-cactus/web/L1CE/issues/82
    if (token_data.l1ce.mode == "read" && !db.databaseChanges[token_data.l1ce.database][token_data.cern.fullname]) {
      console.log(`performing rollback on ${token_data.cern.fullname} being in read mode`);
      db.rollback(token_data, false); // rollback, but don't notify (read mode anyway)
    }
    makeJWTToken(token_data)
      .then(token => ok(token))
      .catch(error => nope(`Error while exchanging token: ${error}`));
  });

  /**
   * Sends a JWT token to the API consumer
   * It decides between sending a HTML and JSON response
   */
  var sendToken = (request, response, token) => {
    if (request.accepts('text/html')) {
      response.setHeader('content-type', 'text/html');
      response.send(htmlfile.replace("<%jwt_token%>", token));
    } else {
      response.send({
        token: token,
        body: jwt.decode(token),
      });
    }
  }

  var getRoles = egroups => Object.keys(config.authentication.roles).filter(roleName => {
    return config.authentication.roles[roleName].some(egroup => egroups.indexOf(egroup) !== -1);
  });

  server.get(`${root}/login/bot`, (request, response) => {
    makeJWTToken({
      l1ce: {
        mode: "write",
        database: "development",
        roles: []
      },
      cern: {
        fullname: "testing bot"
      }
    }, 60*2) // 2min
    .then(token => sendToken(request, response, token))
    .catch(error => response.send(error.statusCode || 500, error.error || error));
  });

  const oauth_config = config.authentication.oauth;
  // detect if default test settings are set
  if (oauth_config.client_id === "myclientid") {
    console.error("oauth is not configured, see authentication.oauth in config.testing.yaml");
    throw new Error("oauth unconfigured");
  }
  if (oauth_config.demo) {
    console.warn("Oauth demo mode enabled");
  } else if (oauth_config.secret) {
    oauth_config.secret = process.env[oauth_config.secret];
    if (!oauth_config.secret) {
      console.error("oauth secret is not configured, no such variable set");
      throw new Error("oauth unconfigured");
    }
  } else {
    oauth_config.secret = fs.readFileSync(config.authentication.oauth.secret_path, "UTF-8").replace("\n", "");
    delete oauth_config.secret_path;
  }
  
  server.get(`${root}/openid`, (request, response) => {
    var proxy_http = !config.proxy.enabled ? http : http.defaults({ proxy: config.proxy.url });

    proxy_http.post('https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token', {
      form: {
        code: request.params.code,
        grant_type: "authorization_code",
        client_secret: oauth_config.secret,
        redirect_uri: oauth_config.redirect_uri,
        client_id: oauth_config.client_id
      },
      json: true
    })
    .then( ({access_token}) => {
      const data = jwt.decode(access_token)
      var token_data = {
        l1ce: {
          mode: "read",
          database: (db.capabilities.development ? "development" : "production"),
          roles: [],
        },
        cern: {
          fullname: data.name,
          username: data.sub,
          id: data.cern_person_id,
          email: data.email,
          first_name: data.given_name,
          last_name: data.family_name,
        }
      };
      
      return makeJWTToken(token_data)
        .then(token => sendToken(request, response, token))
    })
    .catch( error => response.send(error.statusCode || 500, error.error || error));
  });

  server.get(`${root}/login`, (request, response) => {
    /**
     * HAL: Authenticates a user, either by an existing JWT token (used to change privileges) or by redirection to CERN SSO
     * @param mode
     * @param database
     */

    var token_data = {}

    // demo mode
    if (oauth_config.demo) {
      if ((request.params.mode && request.params.mode !== 'read') || (request.params.database && request.params.database !== 'development')) {
        return response.send(403, { code: 403, message: "demo mode doesn't support write mode or the production database" });
      }
      makeJWTToken({
        l1ce: {
          mode: "read",
          database: "development",
          roles: Object.keys(config.authentication.roles)
        },
        cern: {
          fullname: "demo",
          name: "demo"
        }
      }, 60 * 2) // 2min
      .then(token => sendToken(request, response, token))
      .catch(error => response.send(error.statusCode || 500, error.error || error));
    }

    // if an authorization header exists, we're exchanging tokens
    else if (request.headers.authorization) {
      passport.jwt.authenticate(request, response, () => {
        var databaseName = request.user.l1ce.database;
        var userName = request.user.cern.fullname;
        var changesCount = (db.databaseChanges[databaseName][userName] || []).length;
        // it's not allowed to change the session state while there are pending changes
        // renewing an expiring token is allowed
        if (changesCount > 0 && ((request.params.mode && request.params.mode != request.user.l1ce.mode) || (request.params.database && request.params.database != databaseName))) {
          response.send(409, {code: 409, message: "Cannot exchange token while the user has pending changes"})
        }
        else if (userName == "testing bot") {
          response.send(403, {code: 409, message: "Token for testing bot cannot be exchanged"});
        }
        else {
          token_data.l1ce = {
            mode: request.params.mode || request.user.l1ce.mode,
            database: request.params.database || request.user.l1ce.database
          }
          exchangeToken(request.user, token_data.l1ce)
            .then(token => sendToken(request, response, token))
            .catch(error => response.send(503, error));
        }
      });
    }

    else {
      if (oauth_config.redirect_uri.startsWith("/")) {
        const referer = request.header("Referer")
        const host = request.header("Host")
        const isHttps = referer ? referer.startsWith("https:") : config.http.secure
        if (host) {
          oauth_config.redirect_uri = (isHttps ? "https" : "http") + "://" + host + oauth_config.redirect_uri
        } else {
          response.send(401, "cannot infor redirection url");
          return
        }
      }
      response.setHeader('Location', `https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth?client_id=${encodeURIComponent(oauth_config.client_id)}&redirect_uri=${encodeURIComponent(oauth_config.redirect_uri)}&response_type=code&scope=openid+profile+email`);
      response.send(307, {});
    }
  });

  server.get(`${root}/test_timeout`, (request, response) => {});

}
