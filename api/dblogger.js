'use strict';
var db = require.main.require('./database.js');

var rightPad = ( svalue, n ) => {
	return svalue + ' '.repeat( n - svalue.length );
}

var leftPad = '  ';
var keyMaxLen = 0;
var valueMaxLen = 0;
var skipKeys = [
	'ID',
	'VERSION',
	'CREATION_DATE',
	'DESCRIPTION',
	'AUTHOR',
	'ANCESTOR'
];

var getColumnsWitdh = function( tuple ) {
	for ( let key in tuple ) {
		if ( !skipKeys.includes(key) ) {
	        keyMaxLen = key.length > keyMaxLen ? key.length : keyMaxLen;
	        valueMaxLen = tuple[key].length > valueMaxLen ? tuple[key].length : valueMaxLen;
	    }
    }
}

var compare = function( oldTuple, newTuple ) {
	var outtext = '';
	for ( let key in newTuple ) {
		if ( !skipKeys.includes(key) && oldTuple[key] != newTuple[key] ) {
			outtext += leftPad + rightPad( key, keyMaxLen ) + '  :  ' + rightPad( newTuple[key], valueMaxLen ) + '  (was ' + oldTuple[key] + ')\n';
		}
	}
	return outtext;
}

module.exports = (oldKey, newKey, credentials) => new Promise( (ok, nope) => {

	var outtext = '';
	db.getConnection( { credentials : credentials } )
	.then( connection => {
		return Promise.all( [
	            connection,
	            connection.streamExecute(`SELECT * FROM CMS_L1_HLT.L1_HLT_CONF where ID in ( '${oldKey}', '${newKey}' )`)
	          ] )
	})
	.then( ( [connection, {rows: table}] ) => {

	 	var oldTuple = (table[0].ID ==  oldKey) ? table[0] : table[1];
	 	var newTuple = (table[1].ID ==  newKey) ? table[1] : table[0];

		return Promise.all(
		            [ oldTuple,
		              newTuple,
		              ( oldTuple.L1_TRG_CONF_KEY != newTuple.L1_TRG_CONF_KEY ) ?
		                connection.streamExecute(`SELECT * FROM CMS_TRG_L1_CONF.L1_TRG_CONF_KEYS where ID in ( '${oldTuple.L1_TRG_CONF_KEY}', '${newTuple.L1_TRG_CONF_KEY}' ) order by CREATION_DATE`)
		                : Promise.resolve( {rows: []} ),
	 	              ( oldTuple.L1_TRG_RS_KEY != newTuple.L1_TRG_RS_KEY ) ?
	 	                connection.streamExecute(`SELECT * FROM CMS_TRG_L1_CONF.L1_TRG_RS_KEYS where ID in ( '${oldTuple.L1_TRG_RS_KEY}', '${newTuple.L1_TRG_RS_KEY}' ) order by CREATION_DATE`)
	 	                : Promise.resolve( {rows: []} )
	 	            ]
	 	        );

	})
	.then( ( [ oldTuple, newTuple, {rows: confDiff}, {rows: rsDiff } ]) => {

		getColumnsWitdh( newTuple );
		confDiff.length && getColumnsWitdh( confDiff[1] );
		rsDiff.length && getColumnsWitdh( rsDiff[1] );

	 	outtext += '\nFor the following reason:\n';
	 	outtext += leftPad + newTuple.DESCRIPTION + '\n\n';
		outtext += "Differences from the original L1+HLT key:\n"
		outtext += compare( oldTuple, newTuple );
		if ( confDiff.length ) {
			outtext += '\nModified subsystem config key[s]:\n';
			outtext += compare( confDiff[0], confDiff[1] );
		}

		if ( rsDiff.length ) {
			outtext += '\nModified subsystem run settings key[s]:\n';
			outtext += compare( rsDiff[0], rsDiff[1] );
		}

		ok(outtext);

	})
	.catch( error => { nope( outtext += "\n...\nfailed to complete the report\n...\n"); }
	);

});
