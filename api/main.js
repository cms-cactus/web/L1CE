#!/usr/bin/env node
'use strict';
process.chdir(__dirname);

global.prefix = "/api/v0";

const { log } = require('./logger');
global.console = log;
const config = require("./config");
const db = require('./database');
const shutdown = require('./shutdownHandlers');

global.passport = require('passport');

let server = null;
require("./makeserver")
  .then(s => (server = s))
  .then(() => require("./makeWebSocket").register(server))
  .then(() => require("./jwt_auth")(server, global.passport))
  .then(() => db.connect())
  .then(() => require("./API/index")(server))
  .then(() =>
    server.listen(config.http.port, "::", () =>
      console.log(`${server.name} listening at ${server.url}`)
    )
  )
  .catch(error => {
    console.error("Error while starting API", error);
    shutdown();
  });
