var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var cryptokeys = require.main.require('./crypto/cryptokeys.js');

module.exports = (server, passport) => cryptokeys.then(cryptokeys => {
  // server.use(passport.initialize());  
  passport.use(
    new JwtStrategy({
      secretOrKey: cryptokeys.publicKey,
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderWithScheme('jwt'),
        ExtractJwt.fromAuthHeaderWithScheme('bearer')
      ]),
      algorithms: ["RS256"]
    },
      (jwt_data, done) => {
        // jwt_data contains the JWT payload like in https://jwt.io/
        if (jwt_data.l1ce && jwt_data.l1ce.mode && jwt_data.l1ce.database) {
          return done(null, jwt_data);
        } else {
          return done(null, false, "token does not contain L1CE authentication data");
        }
      })
  );

  passport.jwt = {

    authenticate: (request, response, next) => {

      var onResponse = (error, user, info) => {
        if (error) {
          console.error("passport.jwt.authenticate error", error);
          response.send(403, error);

        } else if (!user) {
          var result = {
            code: 401,
            info: info,
            redirect: `${global.prefix}/login`
          };
          if (info && info.message == "No auth token") {
            result.message = "JWT token not found. It must be included in the request headers in the following form: 'Authorization: Bearer <token>'"
          } else {
            result.message = "JWT token is invalid";
          }
          response.send(401, result);
        } else {
          if (!user.l1ce) {
            response.send(403, {
              code: 403,
              info: {},
              message: "This JWT token is not authorized for L1CE. Exchange the token to add authorization data.",
              redirect: `${global.prefix}/login`
            });
          } else {
            request.user = user;
            try {
              next();
            } catch (e) {
              response.send(500, e);
            }
          }
        }
      };

      passport.authenticate('jwt', onResponse)(request, response, next);
    },

    requireReadMode: (request, response, next) => {
      passport.jwt.authenticate(request, response, () => {
        // write mode implies read mode
        if (request.user.l1ce.mode == "read" || request.user.l1ce.mode == "write") {
          try {
            next();
          } catch (e) {
            response.send(500, e);
          }
        } else {
          response.send(403, {
            code: 403,
            info: {},
            message: "this API endpoint requires write mode. Please exchange your token to obtain write permissions.",
            redirect: `${global.prefix}/login`
          });
        }
      });
    },

    requireWriteMode: (request, response, next) => {
      passport.jwt.authenticate(request, response, () => {
        if (request.user.l1ce.mode == "write") {
          try {
            next();
          } catch (e) {
            response.send(500, e);
          }
        } else {
          response.send(403, {
            code: 403,
            info: {},
            message: "this API endpoint requires write mode. Please exchange your token to obtain write permissions.",
            redirect: `${global.prefix}/login`
          });
        }
      });
    },

    requireDevelopmentDatabase: (request, response, next) => {
      passport.jwt.authenticate(request, response, () => {
        if (request.user.l1ce.database == "development") {
          try {
            next();
          } catch (e) {
            response.send(500, e);
          }
        } else {
          response.send(403, {
            code: 403,
            info: {},
            message: "this API endpoint only supports the development database. Please exchange your token to switch to the development database.",
            redirect: `${global.prefix}/login`
          });
        }
      });
    },

    requireProductionDatabase: (request, response, next) => {
      passport.jwt.authenticate(request, response, () => {
        if (request.user.l1ce.database == "production") {
          try {
            next();
          } catch (e) {
            response.send(500, e);
          }
        } else {
          response.send(403, {
            code: 403,
            info: {},
            message: "this API endpoint only supports the production database. Please exchange your token to switch to the production database.",
            redirect: `${global.prefix}/login`
          });
        }
      });
    },

    /*
    * Add this as the first handler function when the standard approach
    * using the 'authorization' header might not be possible
    * This will look in the url for ?token=jwttoken or in the body of the request
    * for the token
    */
    allowTokenInParams: (request, response, next) => {
      if (request.params.token) {
        request.headers['authorization'] = `JWT ${request.params.token}`;
      }
      next();
    }

  }

});
