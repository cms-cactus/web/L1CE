var Promise = require("bluebird");
var config = require("./config");
var mailer = require("./mailer.js");
var oracledb = require('oracledb');
oracledb.Promise = Promise;
var fs = require("fs");
oracledb.autoCommit = false;
oracledb.extendedMetaData = true;
oracledb.externalAuth = false;
oracledb.fetchAsString = [oracledb.NUMBER, oracledb.DATE];
oracledb.maxRows = 0;
oracledb.outFormat = oracledb.OBJECT;
oracledb.poolIncrement = 1;
oracledb.poolMax = 100;
oracledb.poolMin = 0;
oracledb.poolTimeout = 60;
oracledb.prefetchRows = 100;
oracledb.queueRequests = true;
oracledb.queueTimeout = 20000;
oracledb.callTimeout = 20000;
oracledb.stmtCacheSize = 30;
var { io_promise } = require("./makeWebSocket.js");

try {
  fs.statSync('/etc/tnsnames.ora');
} catch (err) {
  const msg = "/etc/tnsnames.ora has not been configured";
  console.error(msg);
  throw new Error(msg);
}

/**
 * A connection pool is created for each configured database user in each configured database
 * i.e.: connectionPools["development"]["someDatabaseUsername"].pool = oracle db pool object
 */
var connectionPools = {};

/**
 * loading configuration parameters
 */
module.exports.capabilities = {};
module.exports.databaseChanges = {};

module.exports.getUsersForDatabase = databaseName => {
  if (!config.databases.users[databaseName]) {
    console.warn(
      `database ${databaseName} is not defined in config.databases.users`
    );
    return config.databases.users.global;
  } else {
    return config.databases.users.global.concat(config.databases.users[databaseName]);
  }
}

module.exports.getConnectionObjectsForDatabase = databaseName => {
  var dbinfo = config.databases.connections[databaseName];

  var arrUsernames = this.getUsersForDatabase(databaseName);
  var connectionObjects = [];
  for (var i = 0; i < arrUsernames.length; i++) {
    var username = arrUsernames[i];
    connectionObjects.push({
      user: username.toLowerCase(),
      password: readPassword(databaseName, username),
      connectString: dbinfo.connectString,
      database: databaseName
    });
  }
  return connectionObjects;
}



/**
 * Creates connection pools for all databases
 */
module.exports.connect = () => Promise.all( Object.keys(config.databases.connections).map(dbName => _connectToDB(dbName)) )

/**
 * Creates connection pools for one database
 * @param {String} databaseName - The name of the database to connect to
 */
var _connectToDB = databaseName => new Promise( (jep, nope) => {
  connectionPools[databaseName] = {};
  module.exports.databaseChanges[databaseName] = {};
  console.info(`connecting to ${databaseName} database`);

  let dbinfo = config.databases.connections[databaseName];
  let errormsgs = Promise.resolve("");

  // get list of users to connect to
  this.getUsersForDatabase(databaseName)
  // compile connection info and attempt to connect
  .map( databaseUserName => {
    try {
      return oracledb.createPool({
        user: databaseUserName.toLowerCase(),
        password: readPassword(databaseName, databaseUserName),
        connectString: dbinfo.connectString,
        database: databaseName
      }).then( pool => {
        connectionPools[databaseName][databaseUserName] = {
          pool: pool,
          connections: {}
        }
        return pool;
      });
    } catch (e) {
      console.error("error while reading password:", e.message)
      return connectionPools[databaseName][databaseUserName] = Promise.reject(`No password is configured for user ${databaseUserName}`)
    }
  })
  // go over each connection and probe for errors
  .forEach( promise => {
    errormsgs = errormsgs.then( msg => new Promise(resolve => {
      promise
      .then( () => resolve(msg) )
      .catch( error => resolve(`${msg}\n${error.message || error}`))
    }))
  })

  errormsgs.then( msg => {
    if (msg == "") {
      console.success(`all connection pools in ${databaseName} database initialized`);
      module.exports.capabilities[databaseName] = true;
      jep(`all connection pools in ${databaseName} database initialized`);
    } else {
      console.error(`not all connection pools in ${databaseName} database initialized: ${msg}`);
      nope(`not all connection pools in ${databaseName} database initialized: ${msg}`);
    }
  })
});


/**
 * opens a new connection
 * @param {Object} args.credentials - Parsed credentials (by passport-jwt) supplied by the user
 * @param {String} [args.databaseUser=cms_trg_r] - The database user to connect as, usually the owner of whatever table you want to access
 * @returns {Promise} A promise that resolves an instance of Connection (https://github.com/oracle/node-oracledb/blob/master/doc/api.md#connectionclass)
 */
module.exports.getConnection = ({credentials, databaseUser="cms_trg_r"}) => {

  var databaseName = credentials.l1ce.database;
  var cernUserName = credentials.cern.fullname;
  var database = connectionPools[databaseName][databaseUser];

  var c = database.connections[cernUserName];
  if (c) {
    return Promise.resolve(c);
  }
  else {
    return database.pool.getConnection()
    // .timeout(10000)
    .then( connection => {
      /*
       * Given the time it takes to make a connection
       * we can easily have a racing condition, check again if we have a connection
       */
      if (database.connections[cernUserName]) {
        // we already have a connection, close this one
        console.warn(`redundant connection made to ${databaseName}.${databaseUser} for CERN user ${cernUserName}, closing`);
        connection.close();
        return database.connections[cernUserName];
      }
      database.connections[cernUserName] = connection;
      module.exports.databaseChanges[databaseName][cernUserName] = [];

      // connection.close must also delete the reference in the pool
      connection.realClose = connection.close;
      connection.close = function() {
        delete database.connections[cernUserName];
        return connection.realClose().then( result => console.debug(`released connection to ${databaseName}.${databaseUser} for CERN user ${cernUserName}`) );
      }

      // a slightly modified version of connection.execute
      // this handles large result objects for you
      connection.streamExecute = (sql, bindParams, options) => new Promise((ok, damn) => {
        var result = {
          rows: [],
          metaData: []
        };
        var stream = connection.queryStream(sql, bindParams, options);
        stream.on('error', function (error) {
          return damn(error);
        });

        // rows come in one by one
        stream.on('data', function (data) {
          result.rows.push(data);
        });

        stream.on('end', function () {
          return ok(result);
        });

        // metaData seems to come all in one go
        stream.on('metadata', function (metaData) {
          result.metaData = metaData;
        });
      });

      connection.clientId = cernUserName;
      connection.module = "L1 Configuration Editor (L1CE)";
      connection.userName = databaseUser;
      console.debug(`connection made to ${databaseName}.${databaseUser} for CERN user ${cernUserName}`);
      return connection;
    });
  }
}

/**
 * Commits all changes performed with given user credentials
 * @param {Object} credentials - Parsed credentials (by passport-jwt) supplied by the user
 * @returns {Promise}
 */

module.exports.commit = credentials => {
  var databaseName = credentials.l1ce.database;
  var cernUserName = credentials.cern.fullname;

  if (cernUserName == "testing bot") {
    return Promise.reject("cannot commit changes for a testing token");
  }
  else {
    console.log(`performing commit for ${cernUserName} in the ${databaseName} database`);

    var database = connectionPools[databaseName];
    // go over all connected database users
    // for each user, check if any of its connections is for our CERN user
    // disconnect all matches, resulting in an implicit rollback
    return Promise.all(Object.keys(database).reduce((r, databaseUser) => {
      let connection = database[databaseUser].connections[cernUserName];
      if (connection) {
        r.push(connection.commit().then(commit => connection.close()));
      }
      return r;
    }, []))
    .then(closedConnections => {

      var listOfChanges = module.exports.databaseChanges[databaseName][cernUserName];
      var leftPad = '  ';

      for (var i = 0; i < listOfChanges.length; i++) {
        var changes = listOfChanges[i];
        var action = "\nUser   '" + changes.author + "'";
        switch (changes.type) {
          case "new":
            action += leftPad + "created new   '" + changes.tableName + "'   key:\n";
            action += leftPad + "'" + changes.keyName + "'  based on  '" + changes.sourceKeyName + "'\n";
            break;
          case "validation":
            action += leftPad + "action in   '" + changes.tableName + "':\n";
            action += leftPad + "restored   '" + changes.keyName + "'\n";
            break;
          case "invalidation":
            action += leftPad + "action in   '" + changes.tableName + "':\n";
            action += leftPad + "decommissioned   '" + changes.keyName + "'\n";
            break;
        }

        if (changes.tableName == 'L1_HLT_CONF' && changes.type == "new") {
          Promise.all([require("./logger.js")(changes.sourceKeyName, changes.keyName, credentials)])
            .then(([output]) => {
              var notify = action + output;
              console.log("COMMIT: ", notify);
              if (notify.length > 0 && config.notifications.emails.length > 0) {
                mailer.sendMail(notify, config.notifications.emails);
              }
            })
        } else {
          console.log("COMMIT: ", action);
        }
      }

      module.exports.databaseChanges[databaseName][cernUserName] = [];
      return io_promise;
    })
    .then(io => io.notifyCommit(credentials))
  }
}
/**
 * Reverts all changes performed with given user credentials
 * @param {Object} credentials - Parsed credentials (by passport-jwt) supplied by the user
 * @returns {Promise}
 */
module.exports.rollback = (credentials, notify=true) => {
  var databaseName = credentials.l1ce.database;
  var cernUserName = credentials.cern.fullname;
  console.log(`performing rollback for ${cernUserName} in the ${databaseName} database`);

  var database = connectionPools[databaseName];
  // go over all connected database users
  // for each user, check if any of its connections is for our CERN user
  // disconnect all matches, resulting in an implicit rollback
  return Promise.all(Object.keys(database).reduce( (r, databaseUser) => {
    let connection = database[databaseUser].connections[cernUserName];
    if (connection) {
      r.push(connection.close());
    }
    return r;
  }, []))
  .then( rollbacks => {
    module.exports.databaseChanges[databaseName][cernUserName] = [];
    if (notify) {
      return io_promise.then(io => io.notifyRollback(credentials));
    }
  })
}

/**
 * Closes all database connections. This implicitly reverts any non-committed changes.
 * @returns {Promise}
 */
module.exports.disconnect = () =>
  // in each database
  Promise.all(Object.keys(connectionPools).map( databaseName => {
    let dbName = databaseName;
    let database = connectionPools[databaseName];
    return Promise.all(
      // for each db user
      Object.keys(database).map( databaseUser => {
        let dbUser = databaseUser;
        let poolManager = database[databaseUser];
        // close all connections in the pool
        if (poolManager.connections) {
          return Promise.all(
            Object.keys(poolManager.connections).map(cernUser =>
              // When a connection is released, any ongoing transaction on the connection is rolled back.
              poolManager.connections[cernUser].close()
            )
          )
            // then close the pool
            .then(closedConnections => poolManager.pool.close().then(() => console.success(`connection pool for ${dbName}.${dbUser} closed`)));
        } else {
          return Promise.resolve();
        }
      })
    )
  }))
  .then( closedDatabases => {
    module.exports.capabilities = {};
    return closedDatabases;
  })

function readPassword(databaseName, username) {
  const strategy = config.databases.secrets.strategy;
  if (!strategy) {
    console.error("no strategy configured to read passwords in config")
    throw new Error("no strategy")
  }
  if (strategy === "filecontent") {
    const p = config.databases.connections[databaseName].secrets.replace(":name", username.toLowerCase())
    return fs.readFileSync(p, "UTF-8").replace("\n", "");
  } else if (strategy === "environment") {
    const key = config.databases.connections[databaseName].secrets.replace(":name", username.toLowerCase())
    const pass = process.env[key]
    if (!pass) {
      throw new Error("no password configured. expected env key '" + key + "' not found")
    }
    return pass
  }
}
