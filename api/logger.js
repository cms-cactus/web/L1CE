const bunyan = require('bunyan');
const p = require('./package');

const log = bunyan.createLogger({
  name: p.name,
  src: true, // prints {src: {file, line}} attributes
  serializers: {
    ...bunyan.stdSerializers,
    error: bunyan.stdSerializers.err,
    errors: errors => errors.map(bunyan.stdSerializers.err),
  },
});

if (process.env.DEBUG) {
  log.level(bunyan.TRACE);
}

const child = log.child({version: p.version});

child.success = (msg, ...args) => {
  child.info({level: 51}, msg, ...args);
}

child.log = child.info;

module.exports.log = child;



const logLogs = [];
const maxLogSize = 100;

const addToLogLogs = str => {
  const l = logLogs.push(str);
  if (l > maxLogSize) {
    logLogs.splice(0, logLogs.length - maxLogSize);
  }
}

const nativeWrite = process.stdout.write;
const nativeErrWrite = process.stderr.write;

process.stdout.write = function(str) {
  nativeWrite.apply(process.stdout, arguments);
  addToLogLogs(str);
}
process.stderr.write = function(str) {
  nativeErrWrite.apply(process.stderr, arguments);
  addToLogLogs(str);
}

module.exports.logHistory = logLogs;