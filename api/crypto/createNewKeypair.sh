#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

function cleanup() {
  rm -rf /tmp/openssl.cnf
}
function fail() {
  >&2 echo $1
  exit ${2:-1}
}
cleanup

OLD_HOSTNAME="l1ce.cms"
NEW_HOSTNAME=$1
CA_CRT_PATH=${2:-}
CA_KEY_PATH=${3:-}
CA_PASS_PATH=${4:-}

echo "creating a key pair for host $NEW_HOSTNAME"
sed "s/$OLD_HOSTNAME/$NEW_HOSTNAME/g" openssl.cnf > /tmp/openssl.cnf

### create key
! openssl genrsa -out /tmp/"$NEW_HOSTNAME".key 4096
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  fail "Key generation failed"
fi

### create certificate request
! openssl req -new -key /tmp/"$NEW_HOSTNAME".key -out /tmp/"$NEW_HOSTNAME".csr -config /tmp/openssl.cnf
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  fail "Certificate request generation failed"
fi

if [[ -z "$CA_CRT_PATH" ]]; then
  echo "Warning: creating a self-signed certificate"
  ! openssl x509 -req -days 730 -sha512 -in /tmp/"$NEW_HOSTNAME".csr -signkey /tmp/"$NEW_HOSTNAME".key -out /tmp/"$NEW_HOSTNAME".crt -extensions req_ext -extfile /tmp/openssl.cnf
else
  ### sign certificate
  ! openssl x509 -req -in /tmp/"$NEW_HOSTNAME".csr -CA $CA_CRT_PATH -CAkey $CA_KEY_PATH -CAcreateserial -out /tmp/"$NEW_HOSTNAME".crt -days 730 -sha512 -extensions req_ext -extfile /tmp/openssl.cnf -passin file:$CA_PASS_PATH
  if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    fail "Certificate signing failed"
  fi
fi

rm -rf /tmp/$NEW_HOSTNAME.csr

echo "done"
echo "key file: /tmp/$NEW_HOSTNAME.key"
echo "cert file: /tmp/$NEW_HOSTNAME.crt"
cleanup
