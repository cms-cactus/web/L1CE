const fs = require("fs");
const { spawn } = require("child_process");

const config = require("../config");

console.log(`generating keypair for hostname ${config.hostname}`);

const job_args = [config.hostname];
if (!config.http.secure || !config.crypto) {
  console.warn("creating a self-signed keypair (config.http.secure != true)");
} else {
  job_args.push(
    config.crypto.ca_crt_path,
    config.crypto.ca_key_path,
    config.crypto.ca_pass_path
  );
}
module.exports = new Promise((ok, nok) => {
  const job = spawn(__dirname + "/createNewKeypair.sh", job_args, {
    cwd: __dirname,
    stdio: "inherit"
  });
  job.on("close", statusCode => {
    if (statusCode === 0) {
      const privateKeyPath = `/tmp/${config.hostname}.key`;
      const publicKeyPath = `/tmp/${config.hostname}.crt`;
      const privateKey = fs.readFileSync(privateKeyPath, "utf-8");
      const publicKey = fs.readFileSync(publicKeyPath, "utf-8");
      fs.unlinkSync(privateKeyPath);
      fs.unlinkSync(publicKeyPath);
      
      console.log("keypair generated");
      ok({ privateKey, publicKey });
    } else {
      nok(new Error("keypair generation failed"));
    }
  });
});
