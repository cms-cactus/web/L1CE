const program = require("commander");
program
  .option("--config-file [path]", "path to the config file to use")
  .parse(process.argv);

module.exports = program;
