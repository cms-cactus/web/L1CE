#/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

ip addr | grep inet | cut -d ' ' -f6 | cut -d '/' -f1
