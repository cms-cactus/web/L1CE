var cryptokeys = require.main.require("./crypto/cryptokeys.js");
var db = require.main.require('./database.js');

let resolve_io = null
module.exports.io_promise = new Promise(ok => resolve_io = ok);
module.exports.register = server => cryptokeys.then(cryptokeys => {
  var io = require('socket.io')(server.server);
  // io.set('transports', ['polling', 'websocket']);

  var jwt = require('jsonwebtoken');

  io.connectedUsers = [];

  require('socketio-auth')(io, {
    authenticate: function (socket, data, callback) {
      var token = jwt.verify(data.token, cryptokeys.publicKey,
        {
          algorithms: ['RS256']
        },
        (error, decoded) => {
          if (error || !decoded) {
            if (error.message !== "invalid signature") {
              console.error("Token Authentication Problem:", error);
            }
            return callback(new Error("invalid JWT token"));
          } else {
            socket.user = decoded;
            return callback(null, true);
          }
        }
      );
    },

    postAuthenticate: function (socket, data) {      
      var userInfo = {
        fullname: socket.user.cern.fullname,
        connectTime: new Date(),
        userAgent: socket.handshake.headers['user-agent'],
        address: socket.handshake.headers['x-forwarded-for'] || socket.handshake.address
      };
      io.connectedUsers.push(userInfo);
      console.info(`User ${userInfo.fullname} connected from ${userInfo.address}`);
      io.emit('user connect', userInfo);

      socket.on('disconnect', () => {
        for (let i = 0; i < io.connectedUsers.length; i++) {
          var someUser = io.connectedUsers[i];
          if (someUser.address == userInfo.address && someUser.connectTime == userInfo.connectTime) {
            io.connectedUsers.splice(i, 1);
            console.info(`User ${userInfo.fullname} disconnected from ${userInfo.address}`);
            io.emit('user disconnect', userInfo);
          }
        }
      });

      /*
      * Only the interface knows what key the new key was based on
      * therefore, the interface will fire this notification after
      * the save was successful
      */
      socket.on('new key saved', changeObj => {
        io.emit('new key saved', changeObj);
      });
    }
  });

  io.notifyInvalidation = ({ tableName, tableOwner, primaryKey, credentials }) => {
    var databaseName = credentials.l1ce.database;
    var userName = credentials.cern.fullname;
    var changeObj = {
      tableName: tableName,
      tableOwner: tableOwner,
      keyName: primaryKey,
      author: userName
    };
    io.emit('invalidation', changeObj);
    changeObj.type = "invalidation";
    db.databaseChanges[databaseName][userName].push(changeObj);
  };

  io.notifyValidation = ({ tableName, tableOwner, primaryKey, credentials }) => {
    var databaseName = credentials.l1ce.database;
    var userName = credentials.cern.fullname;
    var changeObj = {
      tableName: tableName,
      tableOwner: tableOwner,
      keyName: primaryKey,
      author: userName
    };
    io.emit('validation', changeObj);
    changeObj.type = "validation";
    db.databaseChanges[databaseName][userName].push(changeObj);
  };

  /**
   * Invoked by the API when a new table row is created
   */
  io.notifyNewTable = ({ tableName, tableOwner, primaryKey, sourcePrimaryKey, credentials }) => {
    var databaseName = credentials.l1ce.database;
    var userName = credentials.cern.fullname;
    var changeObj = {
      author: userName,
      tableName: tableName,
      tableOwner: tableOwner,
      keyName: decodeURIComponent(primaryKey),
      sourceKeyName: decodeURIComponent(sourcePrimaryKey)
    };
    io.emit('new key saved', changeObj);
    changeObj.type = "new";
    db.databaseChanges[databaseName][userName].push(changeObj);
  };

  io.notifyCommit = credentials => {
    io.emit('commit', credentials.cern.fullname);
  }
  io.notifyRollback = credentials => {
    io.emit('rollback', credentials.cern.fullname);
  }

  resolve_io(io);
  return io;
});
