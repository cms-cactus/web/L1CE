var restify = require('restify');
restify.errors = require('restify-errors');
var fs = require('fs');
var tools = require.main.require('./API/tools');
var cryptokeys = require.main.require("./crypto/cryptokeys");
var restifyJSONHAL = require("restify-json-hal");
var config = require("./config");

module.exports = cryptokeys.then(cryptokeys => {
  var serveropts = { name: 'L1CE' };
  if (config.http.secure) {
    serveropts.certificate = cryptokeys.publicKey;
    serveropts.key = cryptokeys.privateKey;
  }
  var server = restify.createServer(serveropts);
  server.server.setTimeout(60000);
  server.formatters['text/html'] = server.formatters['text/plain'];
  server.use(restify.plugins.gzipResponse());

  const corsMiddleware = require('restify-cors-middleware')
  const cors = corsMiddleware({
    origins: [`${global}`],
    allowHeaders: ['Accept',
      'Accept-Version',
      'Content-Type',
      'Api-Version',
      'X-Requested-With',
      'Authorization',
      'sid',
      'lang',
      'origin',
      'subsystems',
      'withcredentials'],
    exposeHeaders: []
  })

  server.pre(cors.preflight)
  server.use(cors.actual)

  server.use(restifyJSONHAL(server, {
    overrideJSON: true
  }));

  server.use(restify.plugins.queryParser({ mapParams: true }));
  server.use(restify.plugins.bodyParser({ mapParams: true }));
  server.use(restify.plugins.fullResponse());

  const knownRoutes = ['/changes', '/workflows', '/editor', '/compare', '/admin'];
  server.on('NotFound', (request, response, error) => {
    const url = request.url;
    const routeMatch = knownRoutes.some(route => url.startsWith(route));
    if (routeMatch) {
      fs.readFile(__dirname + '/../htdocs/dist/index.html', 'UTF-8', (error, data) => {
        if (error) {
          response.send(500, { code: 500, message: "could not read file" });
        } else {
          response.setHeader('content-type', 'text/html');
          response.send(data);
        }
      });
    } else {
      response.send(404, { code: 404, message: error.message });
    }
  });

  var unknownMethodHandler = (req, res) => {
    if (req.method.toLowerCase() === 'options') {
      if (res.methods.indexOf('OPTIONS') === -1) res.methods.push('OPTIONS');

      res.header('Access-Control-Allow-Credentials', true);
      var allowHeaders = ['Accept', 'Accept-Version', 'Content-Type', 'Api-Version', 'Origin', 'X-Requested-With', 'Authorization'];
      res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
      res.header('Access-Control-Allow-Methods', res.methods.join(', '));
      res.header('Access-Control-Allow-Origin', req.headers.origin);

      return res.send(200);
    } else {
      return res.send(new restify.errors.MethodNotAllowedError());
    }
  }

  server.on('MethodNotAllowed', unknownMethodHandler);

  // remove trailing /, this way our code keeps working as expected
  // e.g. /users/ becomes /users
  server.pre(restify.pre.sanitizePath());

  server.pre(function responseLogger(request, response, next) {
    // https://github.com/restify/node-restify/blob/af2c9f46c9b9fdbc0846943afa916c9194b261b6/lib/response.js#L369
    const realSend = response.__send.bind(response);
    response.__send = function wrappedSend(opts) {
      const httpCode = opts.code || response.statusCode || 200;
      const endpoint = `${request.method} ${request.url}`;
      const params = request.params && JSON.stringify(request.params, null, 2);
      const responseBody = opts.body && JSON.stringify(opts.body.message || opts.body);
      if (httpCode > 401 || httpCode == 400) {
        console.error("Erronous message returned", { httpCode, endpoint, params, response: responseBody });
      }
      realSend(opts);
    }
    let url = request.url;
    if (url.startsWith("/api/")) {
      if (url.startsWith("/api/v0/handle_oauth")) {
        url = "/api/v0/handle_oauth";
      }
      console.info("handling", url);
    }
    return next();
  });

  server.sanitize = function sanitize(request, response, next) {
    // get all `:varname` pieces in endpoint path
    var params = request.route.path.match(/:[a-zA-Z1-9-_]*/g).map(param => param.substring(1))

    // check all those parameters for non-whitelisted characters
    var invalid = params.filter(name => name != "token").some(paramName => !/^[A-Za-z0-9-_:\/]*$/.exec(request.params[paramName]))
    if (!invalid) {
      return next();
    }
    else {
      response.send(400, {code: 400, message: "invalid input"});
      response.end();
    }
  }

  server.sanitizeKeyname = function sanitizeKeyname(request, response, next) {
    // get all `:varname` pieces in endpoint path
    var params = request.route.path.match(/:[a-zA-Z1-9-_]*/g).map(param => param.substring(1))

    // check all those parameters for non-whitelisted characters
    // this list is extended to include non-trigger keynames
    var invalid = params.filter(name => name != "token").some(paramName => !/^[A-Za-z0-9-_.: \/]*$/.exec(request.params[paramName]))
    if (!invalid) {
      return next();
    }
    else {
      response.send(400, {code: 400, message: "invalid input"});
      response.end();
    }
  }

  server.get("/service-worker.js", (request, response) => {
    response.setHeader('Content-Type', 'application/javascript');
    response.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
    response.setHeader('Pragma', 'no-cache');
    response.setHeader('Expires', '0');
    fs.readFile(__dirname + '/../htdocs/dist/service-worker.js', 'UTF-8', (error, data) => {
      if (error) {
        response.send(404, {code: 404, message: error.message});
      } else {
        response.send(data);
      }
    });
  });

  server.get('/docs', (request, response, next) => {
    response.redirect(302, '/docs/openapi/', next);
  });
  server.get(/^\/docs.*/, restify.plugins.serveStatic({
    directory: __dirname + `/..`,
    default: 'index.html'
  }));

  server.get(/^(?!\/api)\/.*/, restify.plugins.serveStatic({
    directory: __dirname + `/../htdocs/dist`,
    default: 'index.html'
  }));

  tools.initAPIRoot(__dirname);

  return server;
});
