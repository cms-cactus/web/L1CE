const path = require('path');
const { configFile, hostname } = require('./discover_config');

const full_path = path.resolve(configFile);
console.log("loading config file", full_path);

const config = require("require-yml")(full_path);
if (!config) {
  throw new Error("Config file not found or empty");
}

config.hostname = hostname;

module.exports = config;
