'use strict';
const nodemailer = require('nodemailer');

module.exports.sendMail = (message, recipients) => {
    let transporter = nodemailer.createTransport({
        sendmail: true,
        newline: 'unix',
        path: '/usr/sbin/sendmail'
    });
    transporter.sendMail({
        from: 'L1 Trigger Configuration Editor <l1ce@cern.ch>',
        to: recipients,
        subject: 'New L1/HLT key created',
        text: message
    }, (err, info) => {
        if ( err ) {
            console.error('sendMail: sending notification to', recipients);
            console.error("sendMail: error sending mail: ", err);
        } else {
            console.log('sendMail: notification sent to', recipients);
        }
    });
  }


