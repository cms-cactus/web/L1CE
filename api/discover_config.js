const os = require('os');
const { spawnSync } = require('child_process');
const cli_input = require('./cli_parse');
const CONF_FOLDER = '../configs'
const HOSTS_FILE = 'hostnames.yaml';
const { hostnames } = require("require-yml")(getBuiltinConfigPath(HOSTS_FILE));

function getBuiltinConfigPath(name) {
  return CONF_FOLDER + "/" + name;
}

module.exports = determineConfig();

function determineConfig() {
  const result = {
    configFile: getBuiltinConfigPath('default.yaml'),
    hostname: os.hostname()
  }
  if (cli_input.configFile) {
    console.info("config file overridden in CLI");
    result.configFile = cli_input.configFile;
  } else if (process.env.CI) {
    console.info("process runs in CI environment");
    result.configFile = getBuiltinConfigPath(hostnames.CI);
  } else if (process.env.L1CE_CONFIG_FILE) {
    console.info("config set using environment variable");
    result.configFile = process.env.L1CE_CONFIG_FILE;
  } else {
    const matchedHost = hostnameFromIP();
    if (matchedHost) {
      result.configFile = getBuiltinConfigPath(hostnames[matchedHost]);
      result.hostname = matchedHost;
    } else {
      console.warn(`no hostname match found, using default. Consider updating ${HOSTS_FILE}`);
    }
  }
  return result;
}

function hostnameFromIP() {
  const local_ips = get_local_ips();
  const matchedHost = Object.keys(hostnames).filter(h => h !== "default").find(hostname => {
    const dns_records = get_dns_records(hostname);
    return dns_records.some(record => local_ips.indexOf(record) !== -1);
  });
  console.success(`hostname ${matchedHost} matches local IP address`);
  return matchedHost;
}

function execute_and_get_lines(command, args, options) {
  const job = spawnSync(command, args, options);

  if (job.status === 0) {
    return job.stdout
      .toString()
      .split(os.EOL)
      .filter(n => n);
  } else {
    console.error(`exit code ${job.status} from command ${command}`, {
      code: job.status,
      error: job.error,
      command, args, options,
      stdout: job.stdout && job.stdout.toString(),
      stderr: job.stderr && job.stderr.toString()
    });
    
    throw new Error(`exit code ${job.status} from command ${command}`);
  }
}

function get_local_ips() {
  return execute_and_get_lines("./get_local_ips.sh");
}

function get_dns_records(hostname) {
  return execute_and_get_lines("dig", ["+short", hostname]);
}
