var db = require("./database.js");

function shutdown({signal = "exit", exitCode = 1} = {}) {
  console.log(`Caught ${signal} signal, halting ... `);
  if (db) {
    db.disconnect()
      .then(result => process.exit())
      .catch(error => {
        if (error.message.startsWith('NJS-065')) { // NJS-065: connection pool was closed
          console.warn('waiting for database connections to close');
        } else {
          console.error("Error while disconnecting database", error);
          process.exit(exitCode);
        }
      });
  } else {
    process.exit(exitCode);
  }
}
["SIGINT", "SIGTERM"].forEach(s => process.on(s, () => shutdown({signal: s, code: 0})));

process.on("unhandledRejection", (reason, promise) => {
  if (
    reason &&
    reason.message &&
    reason.message == "NJS-003: invalid connection"
  ) {
    // some code messed up the database connections, abort
    console.error("database connections invalid, aborting");
    process.abort();
  } else {
    console.error("unhandled rejection!", reason, promise);
  }
});

process.on("uncaughtException", err => {
  console.error("uncaught exception, shutting down", err);
  shutdown({code: 1});
});

module.exports = shutdown;
