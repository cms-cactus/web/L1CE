##
### Docker commands
##

# for testing only
# to publish docker images, please use CI
docker: L1CE-${VERSION}.cc7.x86_64.rpm ## build docker image
	docker build \
	-t gitlab-registry.cern.ch/cms-cactus/web/l1ce:${VERSION} \
	.

docker.shell: ## open a shell in a runner container
	docker-compose -f .makefile/docker-compose.yml run --rm runner bash

docker.%: ## run any make target in a docker container
	docker-compose -f .makefile/docker-compose.yml run runner "make $(subst docker.,,$@)"