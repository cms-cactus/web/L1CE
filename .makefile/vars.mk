##
## Repo version information
##

CI_COMMIT_TAG ?= $(shell git describe --abbrev=0)
CI_COMMIT_SHORT_SHA ?= $(shell git rev-parse --short HEAD)
COMMITS_SINCE_LAST_TAG := $(shell git rev-list ${CI_COMMIT_TAG}..HEAD --count)

ifeq ($(strip $(shell git status --porcelain 2>/dev/null)),)
	GIT_TREE_STATE := clean
else
	GIT_TREE_STATE := dirty
	GIT_DIRTY_POSTFIX := -dirty
endif

VERSION ?= ${CI_COMMIT_TAG}-${COMMITS_SINCE_LAST_TAG}${GIT_DIRTY_POSTFIX}

##
## Version information and locations of external binaries
##

NODE_VERSION=12.18.3
NODE=

##
## Helper variables so we download the right binaries for your machine
##

# Linux: 'linux'
# MacOS: 'darwin'
uname_s := $(shell uname -s | tr '[:upper:]' '[:lower:]')
# x86_64: 'x86_64'
# Apple M1: 'arm64'
uname_m := $(shell uname -m)

NODE_ARCH.x86_64 := x64
NODE_ARCH := $(or ${NODE_ARCH.${uname_m}},${uname_m})
